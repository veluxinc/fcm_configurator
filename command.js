/*when home button clicked -> brings to index webpage*/
function home() {
    location.href = 'index.html';
}

/*when inside button clicked -> brings to inside webpage*/
function inside() {
    location.href = 'inside.html';
}

/*when outside button is clicked -> brings to outside webpage*/
function outside() {
    location.href = 'outside.html';
}

function top_INDEX(){
    location.href = '#top_INDEX'
}

function top_INSIDE(){
    location.href = '#top_INSIDE'
}

function top_OUTSIDE(){
    location.href = '#top_OUTSIDE'
}

function goToTable1(){
    location.href = '#table1';
}

/*button js code*/
//Get the button:
//mybutton = document.getElementById("button-box");

// When the user scrolls down 20px from the top of the document, show the button
/*window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}

// When the user clicks on the button, scroll to the top of the document
function backToTop() {
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
}*/

/*when VIEW RESULTS button FOR INSIDE is clicked -> calls function above and displays information below input section*/
var subButton_inside = document.getElementById('subButton_inside');
if(subButton_inside){
subButton_inside.addEventListener('click', findSkylight_inside, false);
subButton_inside.addEventListener('keypress', findSkylight_inside, false);
subButton_inside.addEventListener('click', goToTable1 , false);
subButton_inside.addEventListener('keypress', goToTable1 , false);

var hit_enter = document.getElementById('heightField');
hit_enter.addEventListener("keydown", function (e) {
    if (e.key === "Enter") {  
        findSkylight_inside(e);
        goToTable1();
    }
}, false);

}

/*when VIEW RESULTS button FOR OUTSIDE is clicked -> calls function above and displays information below input section*/
var subButton_outside = document.getElementById('subButton_outside');
if(subButton_outside){
subButton_outside.addEventListener('click', findSkylight_outside, false);
subButton_outside.addEventListener('keypress', findSkylight_outside, false);
subButton_outside.addEventListener('click', goToTable1 , false);
subButton_outside.addEventListener('keypress', goToTable1 , false);

var hit_enter2 = document.getElementById('heightField');
hit_enter2.addEventListener("keydown", function (e) {
    if (e.key === "Enter") {  
        findSkylight_outside(e);
        goToTable1();
    }
}, false);
}



/*this function finds which skylight the dimensions are for, and can display appropriate information*/
async function findSkylight_inside() {

    var in_out = 'inside';

    var x = document.getElementById('table1');
    x.style.display = "block"
    
    /*upload the required csv file*/
    var rawData = await fetch('FCM_data_for_configurator.csv');
    var data = await rawData.text();

        /*create the arrays but leave them empty*/
        var StockType = []; var StockCode = []; var MSRP_04 = [];
        var MSRP05C = [];   var MSRP_29 = [];   var MSRP_30 = [];

                /*_s means this array is still a string*/
        var InsideW_low_s = []; var InsideW_exact_s = []; var InsideW_high_s = [];
        var InsideH_low_s = []; var InsideH_exact_s = []; var InsideH_high_s = [];

        var OutsideW_low_s = []; var OutsideW_exact_s = []; var OutsideW_high_s = [];
        var OutsideH_low_s = []; var OutsideH_exact_s = []; var OutsideH_high_s = [];

        var InsideW_low = []; var InsideW_exact = []; var InsideW_high = [];
        var InsideH_low = []; var InsideH_exact = []; var InsideH_high = [];

        var OutsideW_low = []; var OutsideW_exact = []; var OutsideW_high = [];
        var OutsideH_low = []; var OutsideH_exact = []; var OutsideH_high = [];

    /*parse the data from the csv*/
    var table = data.split('\n').slice(15);
    table.forEach(elt => {
        
        col = elt.split(',');
        
        StockType.push(col[0]);       StockCode.push(col[1]);         MSRP_04.push(col[2]);
        MSRP05C.push(col[3]);         MSRP_29.push(col[4]);           MSRP_30.push(col[5]);
        InsideW_low_s.push(col[6]);   InsideW_exact_s.push(col[7]);   InsideW_high_s.push(col[8]);
        InsideH_low_s.push(col[9]);   InsideH_exact_s.push(col[10]);  InsideH_high_s.push(col[11]);
        OutsideW_low_s.push(col[12]); OutsideW_exact_s.push(col[13]); OutsideW_high_s.push(col[14]);
        OutsideH_low_s.push(col[15]); OutsideH_exact_s.push(col[16]); OutsideH_high_s.push(col[17]);

        /*turn the measurement arrays into numbers from strings*/
        InsideW_low = InsideW_low_s.map(str => { return Number(str); });
        InsideW_exact = InsideW_exact_s.map(str => { return Number(str); });
        InsideW_high = InsideW_high_s.map(str => { return Number(str); });
        InsideH_low = InsideH_low_s.map(str => { return Number(str); });
        InsideH_exact = InsideH_exact_s.map(str => { return Number(str); });
        InsideH_high = InsideH_high_s.map(str => { return Number(str); });

        OutsideW_low = OutsideW_low_s.map(str => { return Number(str); });
        OutsideW_exact = OutsideW_exact_s.map(str => { return Number(str); });
        OutsideW_high = OutsideW_high_s.map(str => { return Number(str); });
        OutsideH_low = OutsideH_low_s.map(str => { return Number(str); });
        OutsideH_exact = OutsideH_exact_s.map(str => { return Number(str); });
        OutsideH_high = OutsideH_high_s.map(str => { return Number(str); });

        console.log(StockType, StockCode, MSRP_04, MSRP05C, MSRP_29, MSRP_30,
            InsideW_low, InsideW_exact, InsideW_high, InsideH_low, InsideH_exact, InsideH_high);
    });

    /*store the width of skylight*/
    var widthField = document.getElementById('widthField').value;
    /*a tag used to display info in js in html (result1 is for displaying widthfield but we can change the name later)*/
    var result1 = document.getElementById('result1');

    /*store the height of skylight*/
    var heightField = document.getElementById('heightField').value;
    /*a tag used to display info in js in html (result1 is for displaying widthfield but we can change the name later)*/
    var result2 = document.getElementById('result2');

    /*these are where we write what we want to display in html using the id tags and any variable*/
    result1.textContent = widthField;
    result2.textContent = heightField;

    /*variables used to keep track if the measurement was exact, or needed to be approximated*/
    var exact_width = false;    var exact_height = false;
    var loose_width = false;    var loose_height = false;
    var tight_width = false;    var tight_height = false;
    var inStock = false;        var exit = false;
    var idx = -1;

    while(inStock == false && exit == false){
        /*loops through 35 spots in the arrays we choose*/
        for(i = 0; i < InsideW_exact.length; i++){
            /*checks if it fits into a range*/
            if(widthField >= InsideW_low[i] && widthField <= InsideW_high[i] && heightField >= InsideH_low[i] && heightField <= InsideH_high[i])
            {
                inStock = true;
                idx = i;
                break;
            }
        }
        exit = true;
    }

    if(inStock == true){

        if(widthField == InsideW_exact[idx]){exact_width = true;}
        if(heightField == InsideH_exact[idx]){exact_height = true;}
        if(widthField > InsideW_exact[idx]){tight_width = true;}
        if(widthField < InsideW_exact[idx]){loose_width = true;}
        if(heightField > InsideH_exact[idx]){tight_height = true;}
        if(heightField < InsideH_exact[idx]){loose_height = true;}

        /*if true, displays certain information (i just wrote enough identifiable info to test the code)*/
        document.getElementById("stockType").innerHTML = StockType[idx]; 
        document.getElementById("stockCode").innerHTML = 'FCM ' + StockCode[idx]; 

        if (MSRP_04[idx].includes(1) || MSRP_04[idx].includes(2) || MSRP_04[idx].includes(3) || MSRP_04[idx].includes(4) || MSRP_04[idx].includes(5) || MSRP_04[idx].includes(6) || MSRP_04[idx].includes(7) || MSRP_04[idx].includes(8) || MSRP_04[idx].includes(9) || MSRP_04[idx].includes(0)){
            document.getElementById("MSRP_04").innerHTML = '$' + MSRP_04[idx];
        }else{document.getElementById("MSRP_04").innerHTML = MSRP_04[idx];}

        if (MSRP05C[idx].includes(1) || MSRP05C[idx].includes(2) || MSRP05C[idx].includes(3) || MSRP05C[idx].includes(4) || MSRP05C[idx].includes(5) || MSRP05C[idx].includes(6) || MSRP05C[idx].includes(7) || MSRP05C[idx].includes(8) || MSRP05C[idx].includes(9) || MSRP05C[idx].includes(0)){
            document.getElementById("MSRP05C").innerHTML = '$' + MSRP05C[idx];
        }else{document.getElementById("MSRP05C").innerHTML = MSRP05C[idx];}

        if (MSRP_29[idx].includes(1) || MSRP_29[idx].includes(2) || MSRP_29[idx].includes(3) || MSRP_29[idx].includes(4) || MSRP_29[idx].includes(5) || MSRP_29[idx].includes(6) || MSRP_29[idx].includes(7) || MSRP_29[idx].includes(8) || MSRP_29[idx].includes(9) || MSRP_29[idx].includes(0)){
            document.getElementById("MSRP_29").innerHTML = '$' + MSRP_29[idx];
        }else{document.getElementById("MSRP_29").innerHTML = MSRP_29[idx];}

        if (MSRP_30[idx].includes(1) || MSRP_30[idx].includes(2) || MSRP_30[idx].includes(3) || MSRP_30[idx].includes(4) || MSRP_30[idx].includes(5) || MSRP_30[idx].includes(6) || MSRP_30[idx].includes(7) || MSRP_30[idx].includes(8) || MSRP_30[idx].includes(9) || MSRP_30[idx].includes(0)){
            document.getElementById("MSRP_30").innerHTML = '$' + MSRP_30[idx];
        }else{document.getElementById("MSRP_30").innerHTML = MSRP_30[idx];}

        document.getElementById("optWidthIn").innerHTML = InsideW_exact[idx]; 
        document.getElementById("optHeightIn").innerHTML = InsideH_exact[idx];
        document.getElementById("optWidthOut").innerHTML = 'xxx';
        document.getElementById("optHeightOut").innerHTML = 'xxx'; 
        
        var MSCW = OutsideW_high[idx] + 0.125;
        console.log(MSCW);
        var MSCWtxt = MSCW.toString();
        console.log(MSCWtxt);

        var MSCH = OutsideH_high[idx] + 0.125;
        console.log(MSCH);
        var MSCHtxt = MSCH.toString();
        console.log(MSCHtxt);

        console.log(MSCWtxt.includes(".125"));
        console.log(MSCHtxt.includes(".125"));

        if(MSCWtxt.includes(".125") == true){
            console.log(OutsideW_high[idx]);
            document.getElementById("MSCwidth").innerHTML = (OutsideW_high[idx]) + ' 1/8';
        }
        else{
            console.log(OutsideW_high[idx]-0.5);
            document.getElementById("MSCwidth").innerHTML = (OutsideW_high[idx]-0.5) + ' 5/8';
        }

        if(MSCHtxt.includes(".125") == true){
            console.log(OutsideH_high[idx]);
            document.getElementById("MSCheight").innerHTML = (OutsideH_high[idx]) + ' 1/8';
        }
        else{
            console.log(OutsideH_high[idx]-0.5);
            document.getElementById("MSCheight").innerHTML = (OutsideH_high[idx]-0.5) + ' 5/8';
        }


        /*show messages*/
        /*message number -- requirements -- picture(S)*/
        /*1 -- both exact -- optimal fit*/
        if(exact_width == true && exact_height == true){
            document.getElementById("buffer").style.display = "none";
            document.getElementById("resultsbox").style.display = "table";
            msg1(in_out);
        }
        /*2 -- width tight, height exact -- 1) tight fit   2) overtighten screw*/
        else if(tight_width == true && exact_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg2(in_out);
        }
        /*3 -- width exact, height tight -- 1) tight fit  2) overtighten screw*/
        else if(exact_width == true && tight_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg3(in_out);
        }
        /*4 -- width tight, height tight -- 1) tight fit  2) overtighten screw*/
        else if(tight_width == true && tight_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg4(in_out); 
        }
        /*5 -- width loose, height exact -- 1) loose fit  2)  loose fit w/ backer rod  3) overtighten screw*/
        else if(loose_width == true && exact_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg5(in_out);
        }
        /*6 -- width exact, height loose -- 1) loose fit  2)  loose fit w/ backer rod  3) overtighten screw*/
        else if(exact_width == true && loose_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg6(in_out);
        }
        /*7 -- width loose, height loose -- 1) loose fit  2)  loose fit w/ backer rod  3) overtighten screw*/
        else if(loose_width == true && loose_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg7(in_out);
        }
        /*8 -- width tight, height loose -- 1) tight fit  2) overtighten screw  3) loose fit  4) loose fit w/ backer rod*/
        else if(tight_width == true && loose_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg8(in_out);
        }
        /*9 -- width loose, height tight -- 1) tight fit  2) overtighten screw  3) loose fit  4) loose fit w/ backer rod*/
        else if(loose_width == true && tight_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg9(in_out);
        }
    }
    /*if neither, skylight does not exist, order custom*/
    else{
        document.getElementById("buffer").style.display = "block";
        document.getElementById("stockType").innerHTML = ''; 
        document.getElementById("stockCode").innerHTML = '';
        document.getElementById("MSRP_04").innerHTML = '';
        document.getElementById("MSRP05C").innerHTML = '';
        document.getElementById("MSRP_29").innerHTML = '';
        document.getElementById("MSRP_30").innerHTML = '';
      /*  document.getElementById("optWidth").innerHTML = '';
        document.getElementById("optHeight").innerHTML = '';*/
        document.getElementById("optWidthIn").innerHTML = '';
        document.getElementById("optHeightIn").innerHTML = '';
        document.getElementById("optWidthOut").innerHTML = '';
        document.getElementById("optHeightOut").innerHTML = '';
        document.getElementById("MSCwidth").innerHTML = '';
        document.getElementById("MSCheight").innerHTML = '';
        document.getElementById("resultsbox").style.display = "none";
        msg10(in_out);
    }
}

/*this function finds which skylight the dimensions are for, and can display appropriate information*/
async function findSkylight_outside() {

    var in_out = 'outside';

    var x = document.getElementById('table1');
    x.style.display = "block"

    /*upload the required csv file*/
    var rawData = await fetch('FCM_data_for_configurator.csv');
    var data = await rawData.text();

        /*create the arrays but leave them empty*/
        var StockType = []; var StockCode = []; var MSRP_04 = [];
        var MSRP05C = [];   var MSRP_29 = [];   var MSRP_30 = [];

                /*_s means this array is still a string*/
        var InsideW_low_s = []; var InsideW_exact_s = []; var InsideW_high_s = [];
        var InsideH_low_s = []; var InsideH_exact_s = []; var InsideH_high_s = [];

        var OutsideW_low_s = []; var OutsideW_exact_s = []; var OutsideW_high_s = [];
        var OutsideH_low_s = []; var OutsideH_exact_s = []; var OutsideH_high_s = [];

        var InsideW_low = []; var InsideW_exact = []; var InsideW_high = [];
        var InsideH_low = []; var InsideH_exact = []; var InsideH_high = [];

        var OutsideW_low = []; var OutsideW_exact = []; var OutsideW_high = [];
        var OutsideH_low = []; var OutsideH_exact = []; var OutsideH_high = [];

    /*parse the data from the csv*/
    var table = data.split('\n').slice(15);
    table.forEach(elt => {
        
        col = elt.split(',');
        
        StockType.push(col[0]);       StockCode.push(col[1]);         MSRP_04.push(col[2]);
        MSRP05C.push(col[3]);         MSRP_29.push(col[4]);           MSRP_30.push(col[5]);
        InsideW_low_s.push(col[6]);   InsideW_exact_s.push(col[7]);   InsideW_high_s.push(col[8]);
        InsideH_low_s.push(col[9]);   InsideH_exact_s.push(col[10]);  InsideH_high_s.push(col[11]);
        OutsideW_low_s.push(col[12]); OutsideW_exact_s.push(col[13]); OutsideW_high_s.push(col[14]);
        OutsideH_low_s.push(col[15]); OutsideH_exact_s.push(col[16]); OutsideH_high_s.push(col[17]);

        /*turn the measurement arrays into numbers from strings*/
        InsideW_low = InsideW_low_s.map(str => { return Number(str); });
        InsideW_exact = InsideW_exact_s.map(str => { return Number(str); });
        InsideW_high = InsideW_high_s.map(str => { return Number(str); });
        InsideH_low = InsideH_low_s.map(str => { return Number(str); });
        InsideH_exact = InsideH_exact_s.map(str => { return Number(str); });
        InsideH_high = InsideH_high_s.map(str => { return Number(str); });

        OutsideW_low = OutsideW_low_s.map(str => { return Number(str); });
        OutsideW_exact = OutsideW_exact_s.map(str => { return Number(str); });
        OutsideW_high = OutsideW_high_s.map(str => { return Number(str); });
        OutsideH_low = OutsideH_low_s.map(str => { return Number(str); });
        OutsideH_exact = OutsideH_exact_s.map(str => { return Number(str); });
        OutsideH_high = OutsideH_high_s.map(str => { return Number(str); });

        console.log(StockType, StockCode, MSRP_04, MSRP05C, MSRP_29, MSRP_30,
            OutsideW_low, OutsideW_exact, OutsideW_high, OutsideH_low, OutsideH_exact, OutsideH_high);
    });

    /*store the width of skylight*/
    var widthField = document.getElementById('widthField').value;
    /*a tag used to display info in js in html (result1 is for displaying widthfield but we can change the name later)*/
    var result1 = document.getElementById('result1');

    /*store the height of skylight*/
    var heightField = document.getElementById('heightField').value;
    /*a tag used to display info in js in html (result1 is for displaying widthfield but we can change the name later)*/
    var result2 = document.getElementById('result2');

    /*these are where we write what we want to display in html using the id tags and any variable*/
    result1.textContent = widthField;
    result2.textContent = heightField;

    /*variables used to keep track if the measurement was exact, or needed to be approximated*/
    var exact_width = false;    var exact_height = false;
    var loose_width = false;    var loose_height = false;
    var tight_width = false;    var tight_height = false;
    var inStock = false;        var exit = false;
    var idx = -1;

    while(inStock == false && exit == false){
        /*loops through 35 spots in the arrays we choose*/
        for(i = 0; i < OutsideW_exact.length; i++){
            /*checks if it fits into a range*/
            if(widthField >= OutsideW_low[i] && widthField <= OutsideW_high[i] && heightField >= OutsideH_low[i] && heightField <= OutsideH_high[i])
            {
                inStock = true;
                idx = i;
                break;
            }
        }
        exit = true;
    }

    if(inStock == true){

        if(widthField == OutsideW_exact[idx]){exact_width = true;}
        if(heightField == OutsideH_exact[idx]){exact_height = true;}
        if(widthField > OutsideW_exact[idx]){tight_width = true;}
        if(widthField < OutsideW_exact[idx]){loose_width = true;}
        if(heightField > OutsideH_exact[idx]){tight_height = true;}
        if(heightField < OutsideH_exact[idx]){loose_height = true;}

        /*if true, displays certain information (i just wrote enough identifiable info to test the code)*/
        document.getElementById("stockType").innerHTML = StockType[idx]; 
        document.getElementById("stockCode").innerHTML = 'FCM ' + StockCode[idx]; 
        
        if (MSRP_04[idx].includes(1) || MSRP_04[idx].includes(2) || MSRP_04[idx].includes(3) || MSRP_04[idx].includes(4) || MSRP_04[idx].includes(5) || MSRP_04[idx].includes(6) || MSRP_04[idx].includes(7) || MSRP_04[idx].includes(8) || MSRP_04[idx].includes(9) || MSRP_04[idx].includes(0)){
            document.getElementById("MSRP_04").innerHTML = '$' + MSRP_04[idx];
        }else{document.getElementById("MSRP_04").innerHTML = MSRP_04[idx];}

        if (MSRP05C[idx].includes(1) || MSRP05C[idx].includes(2) || MSRP05C[idx].includes(3) || MSRP05C[idx].includes(4) || MSRP05C[idx].includes(5) || MSRP05C[idx].includes(6) || MSRP05C[idx].includes(7) || MSRP05C[idx].includes(8) || MSRP05C[idx].includes(9) || MSRP05C[idx].includes(0)){
            document.getElementById("MSRP05C").innerHTML = '$' + MSRP05C[idx];
        }else{document.getElementById("MSRP05C").innerHTML = MSRP05C[idx];}

        if (MSRP_29[idx].includes(1) || MSRP_29[idx].includes(2) || MSRP_29[idx].includes(3) || MSRP_29[idx].includes(4) || MSRP_29[idx].includes(5) || MSRP_29[idx].includes(6) || MSRP_29[idx].includes(7) || MSRP_29[idx].includes(8) || MSRP_29[idx].includes(9) || MSRP_29[idx].includes(0)){
            document.getElementById("MSRP_29").innerHTML = '$' + MSRP_29[idx];
        }else{document.getElementById("MSRP_29").innerHTML = MSRP_29[idx];}

        if (MSRP_30[idx].includes(1) || MSRP_30[idx].includes(2) || MSRP_30[idx].includes(3) || MSRP_30[idx].includes(4) || MSRP_30[idx].includes(5) || MSRP_30[idx].includes(6) || MSRP_30[idx].includes(7) || MSRP_30[idx].includes(8) || MSRP_30[idx].includes(9) || MSRP_30[idx].includes(0)){
            document.getElementById("MSRP_30").innerHTML = '$' + MSRP_30[idx];
        }else{document.getElementById("MSRP_30").innerHTML = MSRP_30[idx];}

        document.getElementById("optWidthIn").innerHTML = 'xxx'; 
        document.getElementById("optHeightIn").innerHTML = 'xxx';
        document.getElementById("optWidthOut").innerHTML = OutsideW_exact[idx];
        document.getElementById("optHeightOut").innerHTML = OutsideH_exact[idx]; 
        
        var MSCW = OutsideW_high[idx] + 0.125;
        console.log(MSCW);
        var MSCWtxt = MSCW.toString();
        console.log(MSCWtxt);

        var MSCH = OutsideH_high[idx] + 0.125;
        console.log(MSCH);
        var MSCHtxt = MSCH.toString();
        console.log(MSCHtxt);

        console.log(MSCWtxt.includes(".125"));
        console.log(MSCHtxt.includes(".125"));

        if(MSCWtxt.includes(".125") == true){
            console.log(OutsideW_high[idx]);
            document.getElementById("MSCwidth").innerHTML = (OutsideW_high[idx]) + ' 1/8';
        }
        else{
            console.log(OutsideW_high[idx]-0.5);
            document.getElementById("MSCwidth").innerHTML = (OutsideW_high[idx]-0.5) + ' 5/8';
        }

        if(MSCHtxt.includes(".125") == true){
            console.log(OutsideH_high[idx]);
            document.getElementById("MSCheight").innerHTML = (OutsideH_high[idx]) + ' 1/8';
        }
        else{
            console.log(OutsideH_high[idx]-0.5);
            document.getElementById("MSCheight").innerHTML = (OutsideH_high[idx]-0.5) + ' 5/8';
        }


        /*show messages*/
        /*message number -- requirements -- picture(S)*/
        /*1 -- both exact -- optimal fit*/
        if(exact_width == true && exact_height == true){
            document.getElementById("buffer").style.display = "none";
            document.getElementById("resultsbox").style.display = "table";
            msg1(in_out);
        }
        /*2 -- width tight, height exact -- 1) tight fit   2) overtighten screw*/
        else if(tight_width == true && exact_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg2(in_out);
        }
        /*3 -- width exact, height tight -- 1) tight fit  2) overtighten screw*/
        else if(exact_width == true && tight_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg3(in_out);
        }
        /*4 -- width tight, height tight -- 1) tight fit  2) overtighten screw*/
        else if(tight_width == true && tight_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg4(in_out); 
        }
        /*5 -- width loose, height exact -- 1) loose fit  2)  loose fit w/ backer rod  3) overtighten screw*/
        else if(loose_width == true && exact_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg5(in_out);
        }
        /*6 -- width exact, height loose -- 1) loose fit  2)  loose fit w/ backer rod  3) overtighten screw*/
        else if(exact_width == true && loose_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg6(in_out);
        }
        /*7 -- width loose, height loose -- 1) loose fit  2)  loose fit w/ backer rod  3) overtighten screw*/
        else if(loose_width == true && loose_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg7(in_out);
        }
        /*8 -- width tight, height loose -- 1) tight fit  2) overtighten screw  3) loose fit  4) loose fit w/ backer rod*/
        else if(tight_width == true && loose_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg8(in_out);
        }
        /*9 -- width loose, height tight -- 1) tight fit  2) overtighten screw  3) loose fit  4) loose fit w/ backer rod*/
        else if(loose_width == true && tight_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg9(in_out);
        }
    }
    /*if neither, skylight does not exist, order custom*/
    else{
        document.getElementById("buffer").style.display = "block";
        document.getElementById("stockType").innerHTML = ''; 
        document.getElementById("stockCode").innerHTML = '';
        document.getElementById("MSRP_04").innerHTML = '';
        document.getElementById("MSRP05C").innerHTML = '';
        document.getElementById("MSRP_29").innerHTML = '';
        document.getElementById("MSRP_30").innerHTML = ''; 
    /*    document.getElementById("optWidth").innerHTML = '';
        document.getElementById("optHeight").innerHTML = '';*/
        document.getElementById("optWidthIn").innerHTML = '';
        document.getElementById("optHeightIn").innerHTML = '';
        document.getElementById("optWidthOut").innerHTML = '';
        document.getElementById("optHeightOut").innerHTML = '';
        document.getElementById("MSCwidth").innerHTML = '';
        document.getElementById("MSCheight").innerHTML = '';
        document.getElementById("resultsbox").style.display = "none";
        msg10(in_out);
    }
    
}

/*THESE ARE OLD FUNTIONS THAT DO NOT USE CSV FILES... keeping here just in case*/
function findSkylight_inside_notcalled(e) {
    
    var in_out = 'inside';

    /*data set for inside measurements of skylights*/
    /*each array holds 35 skylights and each spot (ex. [0] or [9]) is the same skylight in each array*/
    const StockType = ["STANDARD", "STANDARD", "STANDARD", "STANDARD", "STANDARD", "STANDARD", "STANDARD", 
        "STANDARD", "STANDARD", "STANDARD", "STANDARD", "STANDARD", "STANDARD", "STANDARD", "STANDARD", 
        "NEW", "NEW", "NEW", "NEW", "NEW", "NEW", "NEW", "NEW", "NEW", "NEW", "NEW", "NEW", "NEW", "NEW", "NEW", 
        "NEW", "NEW", "NEW", "NEW", "NEW"];
    const StockCode = [1430, 1446, 2222, 2230, 2234, 2246, 2270, 3030, 3046, 3055, 3072, 3434, 3446, 4646, 4672, 
        190365, 190435, 210455, 225225, 225470, 245245, 245470, 245490, 260540, 280525, 315315, 315470, 315700, 
        315715, 365365, 385385, 385540, 420435, 460470, 460700]
    const MSRP_04 = [251, 266, 235, 243, 257, 252, 485, 321, 448, 880, 967, 421, 485, 527, 1189, 595, 625, 625, 
        535, 736 , 535, 736, 736, 759, 759, 638, 776, 893, 920, 759, 759, 875, 821, 893, 1056]
    const MSRP05C = [216, 231, 209, 217, 231, 226, 459, 273, 405, "custom", "custom", 353, 433, 457, "custom", 
        "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", 
        "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom"]
    const MSRP_29 = ["custom", "custom", 377, "custom", "custom", 410, "custom", "custom", "custom", 
        "custom", "custom", "custom", "custom", 869, "custom", "custom", "custom", "custom", "custom", "custom", 
        "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", 
        "custom", "custom", "custom", "custom"]
    const MSRP_30 = ["custom", "custom", 313, "custom", "custom", 350, "custom", "custom", "custom", 
        "custom", "custom", "custom", "custom", 731, "custom", "custom", "custom", "custom", "custom", "custom", 
        "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", 
        "custom", "custom", "custom", "custom"]
    var InsideW_low = [14, 14, 22, 22, 22, 22, 22, 30, 30, 30, 30, 34, 34, 46, 46, 19, 19, 21, 22.5, 22.5, 24.5, 
        24.5, 24.5, 26, 28, 31.5, 31.5, 31.5, 31.5, 36.5, 38.5, 38.5, 42, 46, 46]
    var InsideW_exact = [14.5, 14.5, 22.5, 22.5, 22.5, 22.5, 22.5, 30.5, 30.5, 30.5, 30.5, 34.5, 34.5, 46.5, 46.5, 
        19.75, 19.75, 21.75, 23.25, 23.25, 25.25, 25.25, 25.25, 26.75, 28.75, 32.25, 32.25, 32.25, 32.25, 37.25, 39.25, 
        39.25, 42.75, 46.75, 46.75]
    var InsideW_high = [15.5, 15.5, 23.5, 23.5, 23.5, 23.5, 23.5, 31.5, 31.5, 31.5, 31.5, 35.5, 35.5, 47.5, 47.5, 
        20.5, 20.5, 22.5, 24, 24, 26, 26, 26, 27.5, 29.5, 33, 33, 33, 33, 38, 40, 40, 43.5, 47.5, 47.5]
    var InsideH_low = [30, 46, 22, 30, 34, 46, 70, 30, 46, 54.25, 72, 34, 46, 46, 72, 36.5, 43.5, 45.5, 22.5, 47, 
        24.5, 47, 49, 54, 52.5, 31.5, 47, 70, 71.5, 36.5, 38.5, 54, 43.5, 47, 70]
    var InsideH_exact = [30.5, 46.5, 22.5, 30.5, 34.5, 46.5, 70.5, 30.5, 46.5, 54.75, 72.25, 34.5, 46.5, 46.5, 
        72.25, 37.25, 44.25, 46.25, 23.25, 47.75, 25.25, 47.75, 49.75, 54.75, 53.25, 32.25, 47.75, 70.75, 72.25, 
        37.25, 39.25, 54.75, 44.25, 47.75, 70.75]
    var InsideH_high = [31.5, 47.5, 23.5, 31.5, 35.5, 47.5, 71.5, 31.5, 47.5, 55.75, 73.5, 35.5, 47.5, 47.5, 73.5, 
        38, 45, 47, 24, 48.5, 26, 48.5, 50.5, 55.5, 54, 33, 48.5, 71.5, 73, 38, 40, 55.5, 45, 48.5, 71.5]
    var OutsideW_low = [17, 17, 25, 25, 25, 25, 33, 33, 33, 33, 37, 37, 49, 49, 22, 22, 24, 25.5, 25.5, 27.5, 27.5, 
        27.5, 29, 31, 34.5, 34.5, 34.5, 34.5, 39.5, 41.5, 41.5, 45, 49, 49]
    var OutsideW_exact = [17.5, 17.5, 25.5, 25.5, 25.5, 25.5, 25.5, 33.5, 33.5, 33.5, 33.5, 37.5, 37.5, 49.5, 49.5, 
        22.75, 22.75, 24.75, 26.25, 26.25, 28.25, 28.25, 28.25, 29.75, 31.75, 35.25, 35.25, 35.25, 35.25, 40.25, 
        42.25, 42.25, 45.75, 49.75, 49.75]
    var OutsideW_high = [18.5, 18.5, 26.5, 26.5, 26.5, 26.5, 26.5, 34.5, 34.5, 34.5, 34.5, 38.5, 38.5, 50.5, 50.5, 
        23.5, 23.5, 25.5, 27, 27, 29, 29, 29, 30.5, 32.5, 36, 36, 36, 36, 41, 43, 43, 46.5, 50.5, 50.5]
    var OutsideH_low = [33, 49, 25, 33, 37, 49, 73, 33, 49, 57, 75, 37, 49, 49, 75, 39.5, 46.5, 48.5, 25.5, 50, 
        27.5, 50, 52, 57, 55.5, 34.5, 50, 73, 74.5, 39.5, 41.5, 57, 46.5, 50, 73]
    var OutsideH_exact = [33.5, 49.5, 25.5, 33.5, 37.5, 49.5, 73.5, 33.5, 49.5, 57.75, 75.25, 37.5, 49.5, 49.5, 
        75.25, 40.25, 47.25, 49.25, 26.25, 50.75, 28.25, 50.75, 52.75, 57.75, 56.25, 35.25, 50.75, 73.75, 75.25, 
        40.25, 42.25, 57.75, 47.25, 50.75, 73.75]
    var OutsideH_high = [34.5, 50.5, 26.5, 34.5, 38.5, 50.5, 74.5, 34.5, 50.5, 59.5, 76.5, 38.5, 50.5, 50.5, 76.5, 
        41, 48, 50, 27, 51.5, 29, 51.5, 53.5, 58.5, 57, 36, 51.5, 74.5, 76, 41, 43, 58.5, 48, 51.5, 74.5]

    /*store the width of skylight*/
    var widthField = document.getElementById('widthField').value;
    console.log(widthField);
    /*a tag used to display info in js in html (result1 is for displaying widthfield but we can change the name later)*/
    var result1 = document.getElementById('result1');

    /*store the height of skylight*/
    
    //var heightField = e.target.value;
    //console.log(heightField);
    //console.log(e.target.value);

    var heightField = document.getElementById('heightField').value;
    console.log(heightField);

    /*a tag used to display info in js in html (result1 is for displaying widthfield but we can change the name later)*/
    var result2 = document.getElementById('result2');

    /*these are where we write what we want to display in html using the id tags and any variable*/
    result1.textContent = widthField;
    result2.textContent = heightField;

    /*variables used to keep track if the measurement was exact, or needed to be approximated*/
    var exact_width = false;    var exact_height = false;
    var loose_width = false;    var loose_height = false;
    var tight_width = false;    var tight_height = false;
    var inStock = false;        var exit = false;
    var idx = -1;

    while(inStock == false && exit == false){
        /*loops through 35 spots in the arrays we choose*/
        for(i = 0; i < InsideW_exact.length; i++){
            /*checks if it fits into a range*/
            if(widthField >= InsideW_low[i] && widthField <= InsideW_high[i] && heightField >= InsideH_low[i] && heightField <= InsideH_high[i])
            {
                inStock = true;
                idx = i;
                break;
            }
        }
        exit = true;
    }

    if(inStock == true){

        if(widthField == InsideW_exact[idx]){exact_width = true;}
        if(heightField == InsideH_exact[idx]){exact_height = true;}
        if(widthField > InsideW_exact[idx]){tight_width = true;}
        if(widthField < InsideW_exact[idx]){loose_width = true;}
        if(heightField > InsideH_exact[idx]){tight_height = true;}
        if(heightField < InsideH_exact[idx]){loose_height = true;}

        /*if true, displays certain information (i just wrote enough identifiable info to test the code)*/
        document.getElementById("stockType").innerHTML = StockType[idx]; 
        document.getElementById("stockCode").innerHTML = 'FCM ' + StockCode[idx]; 
        document.getElementById("MSRP_04").innerHTML = MSRP_04[idx];
        document.getElementById("MSRP05C").innerHTML = MSRP05C[idx];
        document.getElementById("MSRP_29").innerHTML = MSRP_29[idx];
        document.getElementById("MSRP_30").innerHTML = MSRP_30[idx];
        document.getElementById("optWidthIn").innerHTML = InsideW_exact[idx]; 
        document.getElementById("optHeightIn").innerHTML = InsideH_exact[idx];
        document.getElementById("optWidthOut").innerHTML = 'xxx';
        document.getElementById("optHeightOut").innerHTML = 'xxx'; 

        var MSCW = OutsideW_high[idx] + 0.125;
        console.log(MSCW);
        var MSCWtxt = MSCW.toString();
        console.log(MSCWtxt);

        var MSCH = OutsideH_high[idx] + 0.125;
        console.log(MSCH);
        var MSCHtxt = MSCH.toString();
        console.log(MSCHtxt);

        console.log(MSCWtxt.includes(".125"));
        console.log(MSCHtxt.includes(".125"));

        if(MSCWtxt.includes(".125") == true){
            console.log(OutsideW_high[idx]);
            document.getElementById("MSCwidth").innerHTML = (OutsideW_high[idx]) + ' 1/8';
        }
        else{
            console.log(OutsideW_high[idx]-0.5);
            document.getElementById("MSCwidth").innerHTML = (OutsideW_high[idx]-0.5) + ' 5/8';
        }

        if(MSCHtxt.includes(".125") == true){
            console.log(OutsideH_high[idx]);
            document.getElementById("MSCheight").innerHTML = (OutsideH_high[idx]) + ' 1/8';
        }
        else{
            console.log(OutsideH_high[idx]-0.5);
            document.getElementById("MSCheight").innerHTML = (OutsideH_high[idx]-0.5) + ' 5/8';
        }


        /*show messages*/
        /*message number -- requirements -- picture(S)*/
        /*1 -- both exact -- optimal fit*/
        if(exact_width == true && exact_height == true){
            document.getElementById("buffer").style.display = "none";
            document.getElementById("resultsbox").style.display = "table";
            msg1(in_out);
        }
        /*2 -- width tight, height exact -- 1) tight fit   2) overtighten screw*/
        else if(tight_width == true && exact_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg2(in_out);
        }
        /*3 -- width exact, height tight -- 1) tight fit  2) overtighten screw*/
        else if(exact_width == true && tight_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg3(in_out);
        }
        /*4 -- width tight, height tight -- 1) tight fit  2) overtighten screw*/
        else if(tight_width == true && tight_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg4(in_out); 
        }
        /*5 -- width loose, height exact -- 1) loose fit  2)  loose fit w/ backer rod  3) overtighten screw*/
        else if(loose_width == true && exact_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg5(in_out);
        }
        /*6 -- width exact, height loose -- 1) loose fit  2)  loose fit w/ backer rod  3) overtighten screw*/
        else if(exact_width == true && loose_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg6(in_out);
        }
        /*7 -- width loose, height loose -- 1) loose fit  2)  loose fit w/ backer rod  3) overtighten screw*/
        else if(loose_width == true && loose_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg7(in_out);
        }
        /*8 -- width tight, height loose -- 1) tight fit  2) overtighten screw  3) loose fit  4) loose fit w/ backer rod*/
        else if(tight_width == true && loose_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg8(in_out);
        }
        /*9 -- width loose, height tight -- 1) tight fit  2) overtighten screw  3) loose fit  4) loose fit w/ backer rod*/
        else if(loose_width == true && tight_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg9(in_out);
        }
    }
    /*if neither, skylight does not exist, order custom*/
    else{
        document.getElementById("buffer").style.display = "block";
        document.getElementById("stockType").innerHTML = ''; 
        document.getElementById("stockCode").innerHTML = '';
        document.getElementById("MSRP_04").innerHTML = '';
        document.getElementById("MSRP05C").innerHTML = '';
        document.getElementById("MSRP_29").innerHTML = '';
        document.getElementById("MSRP_30").innerHTML = '';
     /*   document.getElementById("optWidth").innerHTML = '';
        document.getElementById("optHeight").innerHTML = '';*/
        document.getElementById("optWidthIn").innerHTML = '';
        document.getElementById("optHeightIn").innerHTML = '';
        document.getElementById("optWidthOut").innerHTML = '';
        document.getElementById("optHeightOut").innerHTML = '';
        document.getElementById("MSCwidth").innerHTML = '';
        document.getElementById("MSCheight").innerHTML = '';
        
        document.getElementById("resultsbox").style.display = "none";
        msg10(in_out);
    }

    var x = document.getElementById("table1");
    x.style.display = "block"

    // buffer writing
    /*const buffer = await iniside_workbook.xlsx.writeBuffer();
    const inside_worksheet = buffer.addWorksheet('New Sheet');
    inside_worksheet.columns = [
        { header: 'Width', key: 'width' },
        { header: 'Height', key: 'height' }
    ];
    const row = inside_worksheet.addRow([widthField, heightField]); 
    
    var data = [widthField, heightField];

    test_inside.xlsx.writeBuffer().then((data) => {
        const blob = new Blob([data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8' });
        saveAs(blob, 'test_inside.xlsx');
      });*/

}

function findSkylight_outside_notcalled() {

    var in_out = 'outside';

    /*data set for inside measurements of skylights*/
    /*each array holds 35 skylights and each spot (ex. [0] or [9]) is the same skylight in each array*/
    const StockType = ["STANDARD", "STANDARD", "STANDARD", "STANDARD", "STANDARD", "STANDARD", "STANDARD", 
        "STANDARD", "STANDARD", "STANDARD", "STANDARD", "STANDARD", "STANDARD", "STANDARD", "STANDARD", 
        "NEW", "NEW", "NEW", "NEW", "NEW", "NEW", "NEW", "NEW", "NEW", "NEW", "NEW", "NEW", "NEW", "NEW", "NEW", 
        "NEW", "NEW", "NEW", "NEW", "NEW"];
    const StockCode = [1430, 1446, 2222, 2230, 2234, 2246, 2270, 3030, 3046, 3055, 3072, 3434, 3446, 4646, 4672, 
        190365, 190435, 210455, 225225, 225470, 245245, 245470, 245490, 260540, 280525, 315315, 315470, 315700, 
        315715, 365365, 385385, 385540, 420435, 460470, 460700]
    const MSRP_04 = [251, 266, 235, 243, 257, 252, 485, 321, 448, 880, 967, 421, 485, 527, 1189, 595, 625, 625, 
        535, 736 , 535, 736, 736, 759, 759, 638, 776, 893, 920, 759, 759, 875, 821, 893, 1056]
    const MSRP05C = [216, 231, 209, 217, 231, 226, 459, 273, 405, "custom", "custom", 353, 433, 457, "custom", 
        "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", 
        "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom"]
    const MSRP_29 = ["custom", "custom", 377, "custom", "custom", 410, "custom", "custom", "custom", 
        "custom", "custom", "custom", "custom", 869, "custom", "custom", "custom", "custom", "custom", "custom", 
        "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", 
        "custom", "custom", "custom", "custom"]
    const MSRP_30 = ["custom", "custom", 313, "custom", "custom", 350, "custom", "custom", "custom", 
        "custom", "custom", "custom", "custom", 731, "custom", "custom", "custom", "custom", "custom", "custom", 
        "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", "custom", 
        "custom", "custom", "custom", "custom"]
    var OutsideW_low = [17, 17, 25, 25, 25, 25, 33, 33, 33, 33, 37, 37, 49, 49, 22, 22, 24, 25.5, 25.5, 27.5, 27.5, 
        27.5, 29, 31, 34.5, 34.5, 34.5, 34.5, 39.5, 41.5, 41.5, 45, 49, 49]
    var OutsideW_exact = [17.5, 17.5, 25.5, 25.5, 25.5, 25.5, 25.5, 33.5, 33.5, 33.5, 33.5, 37.5, 37.5, 49.5, 49.5, 
        22.75, 22.75, 24.75, 26.25, 26.25, 28.25, 28.25, 28.25, 29.75, 31.75, 35.25, 35.25, 35.25, 35.25, 40.25, 
        42.25, 42.25, 45.75, 49.75, 49.75]
    var OutsideW_high = [18.5, 18.5, 26.5, 26.5, 26.5, 26.5, 26.5, 34.5, 34.5, 34.5, 34.5, 38.5, 38.5, 50.5, 50.5, 
        23.5, 23.5, 25.5, 27, 27, 29, 29, 29, 30.5, 32.5, 36, 36, 36, 36, 41, 43, 43, 46.5, 50.5, 50.5]
    var OutsideH_low = [33, 49, 25, 33, 37, 49, 73, 33, 49, 57, 75, 37, 49, 49, 75, 39.5, 46.5, 48.5, 25.5, 50, 
        27.5, 50, 52, 57, 55.5, 34.5, 50, 73, 74.5, 39.5, 41.5, 57, 46.5, 50, 73]
    var OutsideH_exact = [33.5, 49.5, 25.5, 33.5, 37.5, 49.5, 73.5, 33.5, 49.5, 57.75, 75.25, 37.5, 49.5, 49.5, 
        75.25, 40.25, 47.25, 49.25, 26.25, 50.75, 28.25, 50.75, 52.75, 57.75, 56.25, 35.25, 50.75, 73.75, 75.25, 
        40.25, 42.25, 57.75, 47.25, 50.75, 73.75]
    var OutsideH_high = [34.5, 50.5, 26.5, 34.5, 38.5, 50.5, 74.5, 34.5, 50.5, 59.5, 76.5, 38.5, 50.5, 50.5, 76.5, 
        41, 48, 50, 27, 51.5, 29, 51.5, 53.5, 58.5, 57, 36, 51.5, 74.5, 76, 41, 43, 58.5, 48, 51.5, 74.5]

    /*store the width of skylight*/
    var widthField = document.getElementById('widthField').value;
    /*a tag used to display info in js in html (result1 is for displaying widthfield but we can change the name later)*/
    var result1 = document.getElementById('result1');

    /*store the height of skylight*/
    var heightField = document.getElementById('heightField').value;
    /*a tag used to display info in js in html (result1 is for displaying widthfield but we can change the name later)*/
    var result2 = document.getElementById('result2');

    /*these are where we write what we want to display in html using the id tags and any variable*/
    result1.textContent = widthField;
    result2.textContent = heightField;

    /*variables used to keep track if the measurement was exact, or needed to be approximated*/
    var exact_width = false;    var exact_height = false;
    var loose_width = false;    var loose_height = false;
    var tight_width = false;    var tight_height = false;
    var inStock = false;        var exit = false;
    var idx = -1;

    while(inStock == false && exit == false){
        /*loops through 35 spots in the arrays we choose*/
        for(i = 0; i < OutsideW_exact.length; i++){
            /*checks if it fits into a range*/
            if(widthField >= OutsideW_low[i] && widthField <= OutsideW_high[i] && heightField >= OutsideH_low[i] && heightField <= OutsideH_high[i])
            {
                inStock = true;
                idx = i;
                break;
            }
        }
        exit = true;
    }

    if(inStock == true){

        if(widthField == OutsideW_exact[idx]){exact_width = true;}
        if(heightField == OutsideH_exact[idx]){exact_height = true;}
        if(widthField > OutsideW_exact[idx]){tight_width = true;}
        if(widthField < OutsideW_exact[idx]){loose_width = true;}
        if(heightField > OutsideH_exact[idx]){tight_height = true;}
        if(heightField < OutsideH_exact[idx]){loose_height = true;}

        /*if true, displays certain information (i just wrote enough identifiable info to test the code)*/
        document.getElementById("stockType").innerHTML = StockType[idx]; 
        document.getElementById("stockCode").innerHTML = 'FCM ' + StockCode[idx]; 
        document.getElementById("MSRP_04").innerHTML = MSRP_04[idx];
        document.getElementById("MSRP05C").innerHTML = MSRP05C[idx];
        document.getElementById("MSRP_29").innerHTML = MSRP_29[idx];
        document.getElementById("MSRP_30").innerHTML = MSRP_30[idx];
        document.getElementById("optWidthIn").innerHTML = 'xxx'; 
        document.getElementById("optHeightIn").innerHTML = 'xxx';
        document.getElementById("optWidthOut").innerHTML = OutsideW_exact[idx];
        document.getElementById("optHeightOut").innerHTML = OutsideH_exact[idx]; 
        
        var MSCW = OutsideW_high[idx] + 0.125;
        console.log(MSCW);
        var MSCWtxt = MSCW.toString();
        console.log(MSCWtxt);

        var MSCH = OutsideH_high[idx] + 0.125;
        console.log(MSCH);
        var MSCHtxt = MSCH.toString();
        console.log(MSCHtxt);

        console.log(MSCWtxt.includes(".125"));
        console.log(MSCHtxt.includes(".125"));

        if(MSCWtxt.includes(".125") == true){
            console.log(OutsideW_high[idx]);
            document.getElementById("MSCwidth").innerHTML = (OutsideW_high[idx]) + ' 1/8';

        }
        else{
            console.log(OutsideW_high[idx]-0.5);
            document.getElementById("MSCwidth").innerHTML = (OutsideW_high[idx]-0.5) + ' 5/8';
        }

        if(MSCHtxt.includes(".125") == true){
            console.log(OutsideH_high[idx]);
            document.getElementById("MSCheight").innerHTML = (OutsideH_high[idx]) + ' 1/8';
        }
        else{
            console.log(OutsideH_high[idx]-0.5);
            document.getElementById("MSCheight").innerHTML = (OutsideH_high[idx]-0.5) + ' 5/8';
        }


        /*show messages*/
        /*message number -- requirements -- picture(S)*/
        /*1 -- both exact -- optimal fit*/
        if(exact_width == true && exact_height == true){
            document.getElementById("buffer").style.display = "none";
            document.getElementById("resultsbox").style.display = "table";
            msg1(in_out);
        }
        /*2 -- width tight, height exact -- 1) tight fit   2) overtighten screw*/
        else if(tight_width == true && exact_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg2(in_out);
        }
        /*3 -- width exact, height tight -- 1) tight fit  2) overtighten screw*/
        else if(exact_width == true && tight_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg3(in_out);
        }
        /*4 -- width tight, height tight -- 1) tight fit  2) overtighten screw*/
        else if(tight_width == true && tight_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg4(in_out); 
        }
        /*5 -- width loose, height exact -- 1) loose fit  2)  loose fit w/ backer rod  3) overtighten screw*/
        else if(loose_width == true && exact_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg5(in_out);
        }
        /*6 -- width exact, height loose -- 1) loose fit  2)  loose fit w/ backer rod  3) overtighten screw*/
        else if(exact_width == true && loose_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg6(in_out);
        }
        /*7 -- width loose, height loose -- 1) loose fit  2)  loose fit w/ backer rod  3) overtighten screw*/
        else if(loose_width == true && loose_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg7(in_out);
        }
        /*8 -- width tight, height loose -- 1) tight fit  2) overtighten screw  3) loose fit  4) loose fit w/ backer rod*/
        else if(tight_width == true && loose_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg8(in_out);
        }
        /*9 -- width loose, height tight -- 1) tight fit  2) overtighten screw  3) loose fit  4) loose fit w/ backer rod*/
        else if(loose_width == true && tight_height == true){
            document.getElementById("buffer").style.display = "block";
            document.getElementById("resultsbox").style.display = "table";
            msg9(in_out);
        }
    }
    /*if neither, skylight does not exist, order custom*/
    else{
        document.getElementById("buffer").style.display = "block";
        document.getElementById("stockType").innerHTML = ''; 
        document.getElementById("stockCode").innerHTML = '';
        document.getElementById("MSRP_04").innerHTML = '';
        document.getElementById("MSRP05C").innerHTML = '';
        document.getElementById("MSRP_29").innerHTML = '';
        document.getElementById("MSRP_30").innerHTML = '';
     /*   document.getElementById("optWidth").innerHTML = '';
        document.getElementById("optHeight").innerHTML = '';*/
        document.getElementById("optWidthIn").innerHTML = '';
        document.getElementById("optHeightIn").innerHTML = '';
        document.getElementById("optWidthOut").innerHTML = '';
        document.getElementById("optHeightOut").innerHTML = '';
        document.getElementById("MSCwidth").innerHTML = '';
        document.getElementById("MSCheight").innerHTML = '';
        document.getElementById("resultsbox").style.display = "none";
        msg10(in_out);
    }

    var x = document.getElementById("table1");
    x.style.display = "block"
}
