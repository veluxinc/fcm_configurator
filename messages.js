//messages for different types of fits
/*1 -- both exact -- optimal fit (green)*/
function msg1OLD(){
    document.getElementById("results1").style.visibility= "visible";
    document.getElementById("results2").style.visibility= "visible";
    document.getElementById("results3").style.visibility= "visible";
    document.getElementById("results4").style.visibility= "visible";
    document.getElementById("results5").style.visibility= "visible";
    document.getElementById("results6").style.visibility= "visible";
    document.getElementById("results7").style.visibility= "visible";
    document.getElementById("results8").style.visibility= "visible";
    document.getElementById("results9").style.visibility= "visible";
    document.getElementById("box1").style.visibility= "visible";
    document.getElementById("box2").style.visibility= "visible";
    
    document.getElementById("box1").style.backgroundColor = "rgb(152, 204, 152)";
    document.getElementById("box2").style.backgroundColor = "rgb(152, 204, 152)";

  //  document.getElementById("num").innerHTML = '1';
    document.getElementById("goodnews").innerHTML = 'Good News!!';
    document.getElementById("gnmsg").innerHTML = 'Your measurement is either a Standard or In-Stock Custom Item.';
    document.getElementById("seeimg1").innerHTML = 'See Image #1';
    document.getElementById("tf").innerHTML = '';
    document.getElementById("tfp1").innerHTML = '';
    document.getElementById("tfb1").innerHTML = '';
    document.getElementById("tfb2").innerHTML = '';
    document.getElementById("tftip").innerHTML = ''; 
    document.getElementById("lf").innerHTML = '';
    document.getElementById("lfp1").innerHTML = '';
    document.getElementById("lfb1").innerHTML = '';
    document.getElementById("lfb2").innerHTML = '';
    document.getElementById("lftip").innerHTML = '';
    document.getElementById("custom1").innerHTML = '';
    document.getElementById("custom2").innerHTML = '';

    document.getElementById("image#1").innerHTML = 'Image #1';
    document.getElementById("image#2").innerHTML = '';
    document.getElementById("image#3").innerHTML = '';
    document.getElementById("image#4").innerHTML = '';   

    document.getElementById("img1").src = "optimal_fit.PNG";
    document.getElementById("img1").style.display = "inline";
    document.getElementById("img2").src = "";
    document.getElementById("img3").src = "";
    document.getElementById("img4").src = "";
    document.getElementById("img5").src = "";
}

var goodnews;   var gnmsg;    var seeimg1;
    var tf;   var tfp1;  var tfb1;    var tfb2;   var tftip;
    var lf;   var lfp1;   var lfb1;   var lfb2;   var lftip;
    var custom1;    var custom2;

async function msg1test(){
  document.getElementById("results1").style.visibility= "visible";
  document.getElementById("results2").style.visibility= "visible";
  document.getElementById("results3").style.visibility= "visible";
  document.getElementById("results4").style.visibility= "visible";
  document.getElementById("results5").style.visibility= "visible";
  document.getElementById("results6").style.visibility= "visible";
  document.getElementById("results7").style.visibility= "visible";
  document.getElementById("results8").style.visibility= "visible";
  document.getElementById("results9").style.visibility= "visible";
  document.getElementById("box1").style.visibility= "visible";
  document.getElementById("box2").style.visibility= "visible";
  
  document.getElementById("box1").style.backgroundColor = "rgb(152, 204, 152)";
  document.getElementById("box2").style.backgroundColor = "rgb(152, 204, 152)";

  var rawmsg = await fetch('messages.csv');
    var test = await rawmsg.text();


  var table = test.split('\n').slice(1);
  table.forEach(elt => {
      
  for(i = 0; i < 1; i++){
      row = elt.split(',');
      
      goodnews = row[0];
      gnmsg = row[1];
      seeimg1 = row[2];

      tf = row[3];
      tfp1 = row[4];
      tfb1 = row[5];
      tfb2 = row[6];
      tftip = row[7];

      lf = row[8];
      lfp1 = row[9];
      lfb1 = row[10];
      lfb2 = row[11];
      lftip = row[12];

      custom1 = row[13];
      custom2 = row[14];
  }
  })

//  document.getElementById("num").innerHTML = '1';
  document.getElementById("goodnews").innerHTML = goodnews;
  document.getElementById("gnmsg").innerHTML = gnmsg;
  document.getElementById("seeimg1").innerHTML = seeimg1;
  document.getElementById("tf").innerHTML = '';
  document.getElementById("tfp1").innerHTML = '';
  document.getElementById("tfb1").innerHTML = '';
  document.getElementById("tfb2").innerHTML = '';
  document.getElementById("tftip").innerHTML = ''; 
  document.getElementById("lf").innerHTML = '';
  document.getElementById("lfp1").innerHTML = '';
  document.getElementById("lfb1").innerHTML = '';
  document.getElementById("lfb2").innerHTML = '';
  document.getElementById("lftip").innerHTML = '';
  document.getElementById("custom1").innerHTML = '';
  document.getElementById("custom2").innerHTML = '';

  document.getElementById("image#1").innerHTML = 'Image #1';
  document.getElementById("image#2").innerHTML = '';
  document.getElementById("image#3").innerHTML = '';
  document.getElementById("image#4").innerHTML = '';   

  document.getElementById("img1").src = "optimal_fit.PNG";
  document.getElementById("img1").style.display = "inline";
  document.getElementById("img2").src = "";
  document.getElementById("img3").src = "";
  document.getElementById("img4").src = "";
  document.getElementById("img5").src = "";

  console.log(goodnews, gnmsg, seeimg1)
}

/*2 -- width tight, height exact -- 1) tight fit   2) overtighten screw */
function msg2OLD(){
    document.getElementById("results1").style.visibility= "visible";
    document.getElementById("results2").style.visibility= "visible";
    document.getElementById("results3").style.visibility= "visible";
    document.getElementById("results4").style.visibility= "visible";
    document.getElementById("results5").style.visibility= "visible";
    document.getElementById("results6").style.visibility= "visible";
    document.getElementById("results7").style.visibility= "visible";
    document.getElementById("results8").style.visibility= "visible";
    document.getElementById("results9").style.visibility= "visible";
    document.getElementById("box1").style.visibility= "visible";
    document.getElementById("box2").style.visibility= "visible";

    document.getElementById("box1").style.backgroundColor = "rgb(255, 247, 136)";
    document.getElementById("box2").style.backgroundColor = "rgb(255, 247, 136)";

  //  document.getElementById("num").innerHTML = '2';
    document.getElementById("goodnews").innerHTML = 'Good News!!';
    document.getElementById("gnmsg").innerHTML = 'Your measurement is either a Standard or In-Stock Custom Item.';
    document.getElementById("seeimg1").innerHTML = '';
    document.getElementById("tf").innerHTML = 'Tight Fit';

    document.getElementById("msg").innerHTML = "Please note that your iniside width measurement is close to the maximum skylight clearance making for a tight fit. (See image #1) Since the skylight overhangs the outside curb/flashing (like a shoebox lid), we recommend that you verify: <br/>•  That the curb is square <br/>•  That the flashed outside curb dimension will allow the appropriate room so the skylight can fit over the flashed curb to ensure a proper seal of the inner skylight gasket to the top of the curb <br/>Tip: When mounting the included screws, please ensure you do not overtighten and bend the aluminum overhang inwards. (See image #2)"

    /*document.getElementById("tfp1").innerHTML = 'Please note that your Inside width measurement is close to the maximum skylight clearance making for a tight fit. (See image #1) Since the skylight overhangs the outside curb/flashing (like a shoebox lid), we recommend that you verify:';
    document.getElementById("tfb1").innerHTML = '•  That the curb is square';
    document.getElementById("tfb2").innerHTML = '•  That the flashed outside curb dimension will allow the appropriate room so the skylight can fit over the flashed curb to ensure a proper seal of the inner skylight gasket to the top of the curb';
    document.getElementById("tftip").innerHTML = 'Tip: When mounting the included screws, please ensure you do not overtighten and bend the aluminum overhang inwards. (See image #2)'; 
  */
    document.getElementById("lf").innerHTML = '';
    document.getElementById("lfp1").innerHTML = '';
    document.getElementById("lfb1").innerHTML = '';
    document.getElementById("lfb2").innerHTML = '';
    document.getElementById("lftip").innerHTML = '';
    document.getElementById("custom1").innerHTML = '';
    document.getElementById("custom2").innerHTML = '';

    document.getElementById("image#1").innerHTML = 'Image #1';
    document.getElementById("image#2").innerHTML = 'Image #2';
    document.getElementById("image#3").innerHTML = '';
    document.getElementById("image#4").innerHTML = '';

    document.getElementById("img5").src = "";
    document.getElementById("img4").src = "";
    document.getElementById("img1").src = "tight_fit.PNG";
    document.getElementById("img1").style.display = "inline";
    document.getElementById("img3").src = "";
    document.getElementById("img2").src = "overtighten_screw.PNG";
    document.getElementById("img2").style.display = "inline";
}

/*3 -- width exact, height tight -- 1) tight fit  2) overtighten screw */
function msg3OLD(){
    document.getElementById("results1").style.visibility= "visible";
    document.getElementById("results2").style.visibility= "visible";
    document.getElementById("results3").style.visibility= "visible";
    document.getElementById("results4").style.visibility= "visible";
    document.getElementById("results5").style.visibility= "visible";
    document.getElementById("results6").style.visibility= "visible";
    document.getElementById("results7").style.visibility= "visible";
    document.getElementById("results8").style.visibility= "visible";
    document.getElementById("results9").style.visibility= "visible";
    document.getElementById("box1").style.visibility= "visible";
    document.getElementById("box2").style.visibility= "visible";

    document.getElementById("box1").style.backgroundColor = "rgb(255, 247, 136)";
    document.getElementById("box2").style.backgroundColor = "rgb(255, 247, 136)";

  //  document.getElementById("num").innerHTML = '3';
    document.getElementById("goodnews").innerHTML = 'Good News!!';
    document.getElementById("gnmsg").innerHTML = 'Your measurement is either a Standard or In-Stock Custom Item.';
    document.getElementById("seeimg1").innerHTML = '';
    document.getElementById("tf").innerHTML = 'Tight Fit';
    document.getElementById("tfp1").innerHTML = 'Please note that your Inside height measurement is close to the maximum skylight clearance making for a tight fit. (See image #1) Since the skylight overhangs the outside curb/flashing (like a shoebox lid), we recommend that you verify:';
    document.getElementById("tfb1").innerHTML = '•  That the curb is square';
    document.getElementById("tfb2").innerHTML = '•  That the flashed outside curb dimension will allow the appropriate room so the skylight can fit over the flashed curb to ensure a proper seal of the inner skylight gasket to the top of the curb';
    document.getElementById("tftip").innerHTML = 'Tip: When mounting the included screws, please ensure you do not overtighten and bend the aluminum overhang inwards. (See image #2)'; 
    document.getElementById("lf").innerHTML = '';
    document.getElementById("lfp1").innerHTML = '';
    document.getElementById("lfb1").innerHTML = '';
    document.getElementById("lfb2").innerHTML = '';
    document.getElementById("lftip").innerHTML = '';
    document.getElementById("custom1").innerHTML = '';
    document.getElementById("custom2").innerHTML = '';

    document.getElementById("image#1").innerHTML = 'Image #1';
    document.getElementById("image#2").innerHTML = 'Image #2';
    document.getElementById("image#3").innerHTML = '';
    document.getElementById("image#4").innerHTML = '';

    document.getElementById("img5").src = "";
    document.getElementById("img4").src = "";
    document.getElementById("img1").src = "tight_fit.PNG";
    document.getElementById("img1").style.display = "inline";
    document.getElementById("img3").src = "";
    document.getElementById("img2").src = "overtighten_screw.PNG";
    document.getElementById("img2").style.display = "inline";
}

/*4 -- width tight, height tight -- 1) tight fit  2) overtighten screw*/
function msg4OLD(){
    document.getElementById("results1").style.visibility= "visible";
    document.getElementById("results2").style.visibility= "visible";
    document.getElementById("results3").style.visibility= "visible";
    document.getElementById("results4").style.visibility= "visible";
    document.getElementById("results5").style.visibility= "visible";
    document.getElementById("results6").style.visibility= "visible";
    document.getElementById("results7").style.visibility= "visible";
    document.getElementById("results8").style.visibility= "visible";
    document.getElementById("results9").style.visibility= "visible";
    document.getElementById("box1").style.visibility= "visible";
    document.getElementById("box2").style.visibility= "visible";

    document.getElementById("box1").style.backgroundColor = "rgb(255, 247, 136)";
    document.getElementById("box2").style.backgroundColor = "rgb(255, 247, 136)";

  //  document.getElementById("num").innerHTML = '4';
    document.getElementById("goodnews").innerHTML = 'Good News!!';
    document.getElementById("gnmsg").innerHTML = 'Your measurement is either a Standard or In-Stock Custom Item.';
    document.getElementById("seeimg1").innerHTML = '';
    document.getElementById("tf").innerHTML = 'Tight Fit';
    document.getElementById("tfp1").innerHTML = 'Please note that your Inside width and/or height measurements are close to the maximum skylight clearance making for a tight fit. (See image #1) Since the skylight overhangs the outside curb/flashing (like a shoebox lid), we recommend that you verify';
    document.getElementById("tfb1").innerHTML = '•  That the curb is square';
    document.getElementById("tfb2").innerHTML = '•  That the flashed outside curb dimension will allow the appropriate room so the skylight can fit over the flashed curb to ensure a proper seal of the inner skylight gasket to the top of the curb';
    document.getElementById("tftip").innerHTML = 'Tip: When mounting the included screws, please ensure you do not overtighten and bend the aluminum overhang inwards. (See image #2)'; 
    document.getElementById("lf").innerHTML = '';
    document.getElementById("lfp1").innerHTML = '';
    document.getElementById("lfb1").innerHTML = '';
    document.getElementById("lfb2").innerHTML = '';
    document.getElementById("lftip").innerHTML = '';
    document.getElementById("custom1").innerHTML = '';
    document.getElementById("custom2").innerHTML = '';

    document.getElementById("image#1").innerHTML = 'Image #1';
    document.getElementById("image#2").innerHTML = 'Image #2';
    document.getElementById("image#3").innerHTML = '';
    document.getElementById("image#4").innerHTML = '';

    document.getElementById("img5").src = "";
    document.getElementById("img4").src = "";
    document.getElementById("img1").src = "tight_fit.PNG";
    document.getElementById("img1").style.display = "inline";
    document.getElementById("img3").src = "";
    document.getElementById("img2").src = "overtighten_screw.PNG";
    document.getElementById("img2").style.display = "inline";
}

/*5 -- width loose, height exact -- 1) loose fit  2)  loose fit w/ backer rod  3) overtighten screw*/
function msg5OLD(){ //FIXED
    document.getElementById("results1").style.visibility= "visible";
    document.getElementById("results2").style.visibility= "visible";
    document.getElementById("results3").style.visibility= "visible";
    document.getElementById("results4").style.visibility= "visible";
    document.getElementById("results5").style.visibility= "visible";
    document.getElementById("results6").style.visibility= "visible";
    document.getElementById("results7").style.visibility= "visible";
    document.getElementById("results8").style.visibility= "visible";
    document.getElementById("results9").style.visibility= "visible";
    document.getElementById("box1").style.visibility= "visible";
    document.getElementById("box2").style.visibility= "visible";

    document.getElementById("box1").style.backgroundColor = "rgb(255, 247, 136)";
    document.getElementById("box2").style.backgroundColor = "rgb(255, 247, 136)";

  //  document.getElementById("num").innerHTML = '5';
    document.getElementById("goodnews").innerHTML = 'Good News!!';
    document.getElementById("gnmsg").innerHTML = 'Your measurement is either a Standard or In-Stock Custom Item.';
    document.getElementById("seeimg1").innerHTML = '';
    document.getElementById("tf").innerHTML = '';
    document.getElementById("tfp1").innerHTML = '';
    document.getElementById("tfb1").innerHTML = '';
    document.getElementById("tfb2").innerHTML = '';
    document.getElementById("tftip").innerHTML = ''; 
    document.getElementById("lf").innerHTML = 'Loose Fit';
    document.getElementById("lfp1").innerHTML = 'Please note that your Inside width measurement is close to the minimum curb dimension, leaving a gap between the skylight overhang and the flashing. (See image #1) To ensure you have a proper seal, we recommend:';
    document.getElementById("lfb1").innerHTML = '•  Properly centering the skylight side-to-side and top-to-bottom on the curb to maximize coverage of the inner gasket on top of the curb';
    document.getElementById("lfb2").innerHTML = '•  Where there is a large gap (>1/2”), using a foam backer rod tucked between the flashing and skylight overhang. (See image#2)';
    document.getElementById("lftip").innerHTML = 'Tip: When mounting the included screws, please ensure you do not overtighten and bend the aluminum overhang inwards. (See image #3)';
    document.getElementById("custom1").innerHTML = '';
    document.getElementById("custom2").innerHTML = '';

    document.getElementById("image#1").innerHTML = 'Image #1';
    document.getElementById("image#2").innerHTML = 'Image #2';
    document.getElementById("image#3").innerHTML = 'Image #3';
    document.getElementById("image#4").innerHTML = '';

    document.getElementById("img5").src = "";
    document.getElementById("img1").src = "loose_fit.PNG";
    document.getElementById("img1").style.display = "inline";
    document.getElementById("img4").src = "";
    document.getElementById("img2").src = "loose_fit_w_backer_rod.PNG";
    document.getElementById("img2").style.display = "inline";
    document.getElementById("img3").src = "overtighten_screw.PNG";
    document.getElementById("img3").style.display = "inline";
}

/*6 -- width exact, height loose -- 1) loose fit  2)  loose fit w/ backer rod  3) overtighten screw*/
function msg6OLD(){
    document.getElementById("results1").style.visibility= "visible";
    document.getElementById("results2").style.visibility= "visible";
    document.getElementById("results3").style.visibility= "visible";
    document.getElementById("results4").style.visibility= "visible";
    document.getElementById("results5").style.visibility= "visible";
    document.getElementById("results6").style.visibility= "visible";
    document.getElementById("results7").style.visibility= "visible";
    document.getElementById("results8").style.visibility= "visible";
    document.getElementById("results9").style.visibility= "visible";
    document.getElementById("box1").style.visibility= "visible";
    document.getElementById("box2").style.visibility= "visible";

    document.getElementById("box1").style.backgroundColor = "rgb(255, 247, 136)";
    document.getElementById("box2").style.backgroundColor = "rgb(255, 247, 136)";

  //  document.getElementById("num").innerHTML = '6';
    document.getElementById("goodnews").innerHTML = 'Good News!!';
    document.getElementById("gnmsg").innerHTML = 'Your measurement is either a Standard or In-Stock Custom Item.';
    document.getElementById("seeimg1").innerHTML = '';
    document.getElementById("tf").innerHTML = '';
    document.getElementById("tfp1").innerHTML = '';
    document.getElementById("tfb1").innerHTML = '';
    document.getElementById("tfb2").innerHTML = '';
    document.getElementById("tftip").innerHTML = ''; 
    document.getElementById("lf").innerHTML = 'Loose Fit';
    document.getElementById("lfp1").innerHTML = 'Please note that your Inside height measurement is close to the minimum curb dimension, leaving a gap between the skylight overhang and the flashing. (See image #1) To ensure you have a proper seal, we recommend:';
    document.getElementById("lfb1").innerHTML = '•  Properly centering the skylight side-to-side and top-to-bottom on the curb to maximize coverage of the inner gasket on top of the curb';
    document.getElementById("lfb2").innerHTML = '•  Where there is a large gap (>1/2”), using a foam backer rod tucked between the flashing and skylight overhang. (See image#2)';
    document.getElementById("lftip").innerHTML = 'Tip: When mounting the included screws, please ensure you do not overtighten and bend the aluminum overhang inwards. (See image #3)';
    document.getElementById("custom1").innerHTML = '';
    document.getElementById("custom2").innerHTML = '';

    document.getElementById("image#1").innerHTML = 'Image #1';
    document.getElementById("image#2").innerHTML = 'Image #2';
    document.getElementById("image#3").innerHTML = 'Image #3';
    document.getElementById("image#4").innerHTML = '';

    document.getElementById("img5").src = "";
    document.getElementById("img1").src = "loose_fit.PNG";
    document.getElementById("img1").style.display = "inline";
    document.getElementById("img4").src = "";
    document.getElementById("img2").src = "loose_fit_w_backer_rod.PNG";
    document.getElementById("img2").style.display = "inline";
    document.getElementById("img3").src = "overtighten_screw.PNG";
    document.getElementById("img3").style.display = "inline";
}

/*7 -- width loose, height loose -- 1) loose fit  2)  loose fit w/ backer rod  3) overtighten screw*/
function msg7OLD(){
    document.getElementById("results1").style.visibility= "visible";
    document.getElementById("results2").style.visibility= "visible";
    document.getElementById("results3").style.visibility= "visible";
    document.getElementById("results4").style.visibility= "visible";
    document.getElementById("results5").style.visibility= "visible";
    document.getElementById("results6").style.visibility= "visible";
    document.getElementById("results7").style.visibility= "visible";
    document.getElementById("results8").style.visibility= "visible";
    document.getElementById("results9").style.visibility= "visible";
    document.getElementById("box1").style.visibility= "visible";
    document.getElementById("box2").style.visibility= "visible";

    document.getElementById("box1").style.backgroundColor = "rgb(255, 247, 136)";
    document.getElementById("box2").style.backgroundColor = "rgb(255, 247, 136)";

   // document.getElementById("num").innerHTML = '7';
    document.getElementById("goodnews").innerHTML = 'Good News!!';
    document.getElementById("gnmsg").innerHTML = 'Your measurement is either a Standard or In-Stock Custom Item.';
    document.getElementById("seeimg1").innerHTML = '';
    document.getElementById("tf").innerHTML = '';
    document.getElementById("tfp1").innerHTML = '';
    document.getElementById("tfb1").innerHTML = '';
    document.getElementById("tfb2").innerHTML = '';
    document.getElementById("tftip").innerHTML = ''; 
    document.getElementById("lf").innerHTML = 'Loose Fit';
    document.getElementById("lfp1").innerHTML = 'Please note that your Inside width and/or height measurements are close to the minimum curb dimension, leaving a gap between the skylight overhang and the flashing. (See image #1) To ensure you have a proper seal, we recommend:';
    document.getElementById("lfb1").innerHTML = '•  Properly centering the skylight side-to-side and top-to-bottom on the curb to maximize coverage of the inner gasket on top of the curb';
    document.getElementById("lfb2").innerHTML = '•  Where there is a large gap (>1/2”), using a foam backer rod tucked between the flashing and skylight overhang. (See image#2)';
    document.getElementById("lftip").innerHTML = 'Tip: When mounting the included screws, please ensure you do not overtighten and bend the aluminum overhang inwards. (See image #3)';
    document.getElementById("custom1").innerHTML = '';
    document.getElementById("custom2").innerHTML = '';

    document.getElementById("image#1").innerHTML = 'Image #1';
    document.getElementById("image#2").innerHTML = 'Image #2';
    document.getElementById("image#3").innerHTML = 'Image #3';
    document.getElementById("image#4").innerHTML = '';

    document.getElementById("img5").src = "";
    document.getElementById("img1").src = "loose_fit.PNG";
    document.getElementById("img1").style.display = "inline";
    document.getElementById("img4").src = "";
    document.getElementById("img2").src = "loose_fit_w_backer_rod.PNG";
    document.getElementById("img2").style.display = "inline";
    document.getElementById("img3").src = "overtighten_screw.PNG";
    document.getElementById("img3").style.display = "inline";
}

/*8 -- width tight, height loose -- 1) tight fit  2) overtighten screw  3) loose fit  4) loose fit w/ backer rod*/
function msg8OLD(){
    document.getElementById("results1").style.visibility= "visible";
    document.getElementById("results2").style.visibility= "visible";
    document.getElementById("results3").style.visibility= "visible";
    document.getElementById("results4").style.visibility= "visible";
    document.getElementById("results5").style.visibility= "visible";
    document.getElementById("results6").style.visibility= "visible";
    document.getElementById("results7").style.visibility= "visible";
    document.getElementById("results8").style.visibility= "visible";
    document.getElementById("results9").style.visibility= "visible";
    document.getElementById("box1").style.visibility= "visible";
    document.getElementById("box2").style.visibility= "visible";

    document.getElementById("box1").style.backgroundColor = "rgb(255, 247, 136)";
    document.getElementById("box2").style.backgroundColor = "rgb(255, 247, 136)";

  //  document.getElementById("num").innerHTML = '8';
    document.getElementById("goodnews").innerHTML = 'Good News!!';
    document.getElementById("gnmsg").innerHTML = 'Your measurement is either a Standard or In-Stock Custom Item.';
    document.getElementById("seeimg1").innerHTML = '';
    document.getElementById("tf").innerHTML = 'Tight Fit';
    document.getElementById("tfp1").innerHTML = 'Please note that your Inside width measurement is close to the maximum skylight clearance making for a tight fit. (See image #1) Since the skylight overhangs the outside curb/flashing (like a shoebox lid), we recommend that you verify:';
    document.getElementById("tfb1").innerHTML = '•  That the curb is square';
    document.getElementById("tfb2").innerHTML = '•  That the flashed outside curb dimension will allow the appropriate room so the skylight can fit over the flashed curb to ensure a proper seal of the inner skylight gasket to the top of the curb';
    document.getElementById("tftip").innerHTML = 'Tip: When mounting the included screws, please ensure you do not overtighten and bend the aluminum overhang inwards. (See image #2)'; 
    document.getElementById("lf").innerHTML = 'Loose Fit';
    document.getElementById("lfp1").innerHTML = 'Please note that your Inside height measurement is close to the minimum curb dimension, leaving a gap between the skylight overhang and the flashing. (See image #3) To ensure you have a proper seal, we recommend:';
    document.getElementById("lfb1").innerHTML = '•  Properly centering the skylight side-to-side and top-to-bottom on the curb to maximize coverage of the inner gasket on top of the curb';
    document.getElementById("lfb2").innerHTML = '•  Where there is a large gap (>1/2”), using a foam backer rod tucked between the flashing and skylight overhang. (See image#4)';
    document.getElementById("lftip").innerHTML = 'Tip: When mounting the included screws, please ensure you do not overtighten and bend the aluminum overhang inwards. (See image #2)';
    document.getElementById("custom1").innerHTML = '';
    document.getElementById("custom2").innerHTML = '';

    document.getElementById("image#1").innerHTML = 'Image #1';
    document.getElementById("image#2").innerHTML = 'Image #2';
    document.getElementById("image#3").innerHTML = 'Image #3';
    document.getElementById("image#4").innerHTML = 'Image #4';

    document.getElementById("img5").src = "";
    document.getElementById("img3").src = "loose_fit.PNG";
    document.getElementById("img3").style.display = "inline";
    document.getElementById("img1").src = "tight_fit.PNG";
    document.getElementById("img1").style.display = "inline";
    document.getElementById("img4").src = "loose_fit_w_backer_rod.PNG";
    document.getElementById("img4").style.display = "inline";
    document.getElementById("img2").src = "overtighten_screw.PNG";
    document.getElementById("img2").style.display = "inline";
}

/*9 -- width loose, height tight -- 1) tight fit  2) overtighten screw  3) loose fit  4) loose fit w/ backer rod*/
function msg9OLD(){
    document.getElementById("results1").style.visibility= "visible";
    document.getElementById("results2").style.visibility= "visible";
    document.getElementById("results3").style.visibility= "visible";
    document.getElementById("results4").style.visibility= "visible";
    document.getElementById("results5").style.visibility= "visible";
    document.getElementById("results6").style.visibility= "visible";
    document.getElementById("results7").style.visibility= "visible";
    document.getElementById("results8").style.visibility= "visible";
    document.getElementById("results9").style.visibility= "visible";

    document.getElementById("box1").style.visibility= "visible";
    document.getElementById("box2").style.visibility= "visible";

    document.getElementById("box1").style.backgroundColor = "rgb(255, 247, 136)";
    document.getElementById("box2").style.backgroundColor = "rgb(255, 247, 136)";

   // document.getElementById("num").innerHTML = '9';
    document.getElementById("goodnews").innerHTML = 'Good News!!';
    document.getElementById("gnmsg").innerHTML = 'Your measurement is either a Standard or In-Stock Custom Item.';
    document.getElementById("seeimg1").innerHTML = '';
    document.getElementById("tf").innerHTML = 'Tight Fit';
    document.getElementById("tfp1").innerHTML = 'Please note that your Inside height measurement is close to the maximum skylight clearance making for a tight fit. (See image #4) Since the skylight overhangs the outside curb/flashing (like a shoebox lid), we recommend that you verify:';
    document.getElementById("tfb1").innerHTML = '•  That the curb is square';
    document.getElementById("tfb2").innerHTML = '•  That the flashed outside curb dimension will allow the appropriate room so the skylight can fit over the flashed curb to ensure a proper seal of the inner skylight gasket to the top of the curb';
    document.getElementById("tftip").innerHTML = 'Tip: When mounting the included screws, please ensure you do not overtighten and bend the aluminum overhang inwards. (See image #3)'; 
    document.getElementById("lf").innerHTML = 'Loose Fit';
    document.getElementById("lfp1").innerHTML = 'Please note that your Inside width measurement is close to the minimum curb dimension, leaving a gap between the skylight overhang and the flashing. (See image #1) To ensure you have a proper seal, we recommend:';
    document.getElementById("lfb1").innerHTML = '•  Properly centering the skylight side-to-side and top-to-bottom on the curb to maximize coverage of the inner gasket on top of the curb';
    document.getElementById("lfb2").innerHTML = '•  Where there is a large gap (>1/2”), using a foam backer rod tucked between the flashing and skylight overhang. (See image#2)';
    document.getElementById("lftip").innerHTML = 'Tip: When mounting the included screws, please ensure you do not overtighten and bend the aluminum overhang inwards. (See image #3)';
    document.getElementById("custom1").innerHTML = '';
    document.getElementById("custom2").innerHTML = '';

    document.getElementById("image#1").innerHTML = 'Image #1';
    document.getElementById("image#2").innerHTML = 'Image #2';
    document.getElementById("image#3").innerHTML = 'Image #3';
    document.getElementById("image#4").innerHTML = 'Image #4';

    document.getElementById("img5").src = "";
    document.getElementById("img1").src = "loose_fit.PNG";
    document.getElementById("img1").style.display = "inline";
    document.getElementById("img3").src = "tight_fit.PNG";
    document.getElementById("img3").style.display = "inline";
    document.getElementById("img2").src = "loose_fit_w_backer_rod.PNG";
    document.getElementById("img2").style.display = "inline";
    document.getElementById("img4").src = "overtighten_screw.PNG";
    document.getElementById("img4").style.display = "inline";
}

/*if neither, skylight does not exist, order custom*/
function msg10OLD(){
    document.getElementById("results1").style.visibility= "hidden";
    document.getElementById("results2").style.visibility= "hidden";
    document.getElementById("results3").style.visibility= "hidden";
    document.getElementById("results4").style.visibility= "hidden";
    document.getElementById("results5").style.visibility= "hidden";
    document.getElementById("results6").style.visibility= "hidden";
    document.getElementById("results7").style.visibility= "hidden";
    document.getElementById("results8").style.visibility= "hidden";
    document.getElementById("results9").style.visibility= "hidden";
    document.getElementById("box1").style.visibility= "visible";
    document.getElementById("box2").style.visibility= "visible";

   /* document.getElementById("results").style.display= "block";
    document.getElementById("results1").style.display= "block";
    document.getElementById("results2").style.display= "block";
    document.getElementById("results3").style.display= "block";
    document.getElementById("results4").style.display= "block";
    document.getElementById("results5").style.display= "block";
    document.getElementById("results6").style.display= "block";
    document.getElementById("results7").style.display= "block";
    document.getElementById("results8").style.display= "block";
    document.getElementById("results9").style.display= "block";*/

    document.getElementById("box1").style.backgroundColor = "rgb(249, 141, 141)";
    document.getElementById("box2").style.backgroundColor = "rgb(249, 141, 141)";

   // document.getElementById("num").innerHTML = '10';
    document.getElementById("goodnews").innerHTML = '';
    document.getElementById("gnmsg").innerHTML = '';
    document.getElementById("seeimg1").innerHTML = '';
    document.getElementById("tf").innerHTML = '';
    document.getElementById("tfp1").innerHTML = '';
    document.getElementById("tfb1").innerHTML = '';
    document.getElementById("tfb2").innerHTML = '';
    document.getElementById("tftip").innerHTML = ''; 
    document.getElementById("lf").innerHTML = '';
    document.getElementById("lfp1").innerHTML = '';
    document.getElementById("lfb1").innerHTML = '';
    document.getElementById("lfb2").innerHTML = '';
    document.getElementById("lftip").innerHTML = '';
    document.getElementById("custom1").innerHTML = 'Unfortunately, your item is not a stock item.';
    document.getElementById("custom2").innerHTML = 'A custom order is required.';

    document.getElementById("image#1").innerHTML = '';
    document.getElementById("image#2").innerHTML = '';
    document.getElementById("image#3").innerHTML = '';
    document.getElementById("image#4").innerHTML = '';

    document.getElementById("img1").src = "";
    document.getElementById("img2").src = "";
    document.getElementById("img3").src = "";
    document.getElementById("img4").src = "";
    document.getElementById("img5").src = "";
}
