var msgnum = [];
var instock_custom = [];
var msg_1 = [];
var msg_2 = [];

async function getMessages(){
  var rawData = await fetch('NEWmessages.csv');
  var data = await rawData.text();

  var table = data.split('\n').slice(1);
  table.forEach(elt => {
      
      col = elt.split(',');
      
      msgnum.push(col[0])
      instock_custom.push(col[1]);
      msg_1.push(col[2]);
      msg_2.push(col[3]);

      /*for(i = 0; i < instock_custom.length; i++){
          if(instock_custom[i] == NULL){instock_custom[i] = ''}
          if(msg_1[i] == NULL){instock_custom[i] = ''}
          if(msg_2[i] == NULL){instock_custom[i] = ''}
      }*/
  });
  console.log(instock_custom[0], msg_1[0], msg_2[0]);
  console.log(instock_custom[1], msg_1[1], msg_2[1]);
  console.log(instock_custom[8], msg_1[8], msg_2[8]);
}


//messages for different types of fits
/*1 -- both exact -- optimal fit (green)*/
function msg1(inside_outside){
//  getMessages();
    document.getElementById("results1").style.visibility= "visible";
    document.getElementById("results2").style.visibility= "visible";
    document.getElementById("results3").style.visibility= "visible";
    document.getElementById("results4").style.visibility= "visible";
    document.getElementById("results5").style.visibility= "visible";
    document.getElementById("results6").style.visibility= "visible";
    document.getElementById("results7").style.visibility= "visible";
    document.getElementById("results8").style.visibility= "visible";
    document.getElementById("results9").style.visibility= "visible";
    document.getElementById("box1").style.visibility= "visible";
    document.getElementById("box2").style.visibility= "visible";
    
    document.getElementById("box1").style.backgroundColor = "rgb(152, 204, 152)";
    document.getElementById("box2").style.backgroundColor = "rgb(152, 204, 152)";

  //  document.getElementById("num").innerHTML = '1';
    document.getElementById("instock/custom").innerHTML = 'Good News!!<br/> Your measurement is either a Standard or In-Stock Custom Item.';
    document.getElementById("msg1").innerHTML = 'See Image #1';
    document.getElementById("msg2").innerHTML = '';

  /*  document.getElementById("instock/custom").innerHTML = instock_custom[0];
    document.getElementById("msg1").innerHTML = msg_1[0];
    document.getElementById("msg2").innerHTML = msg_2[0];*/

    document.getElementById("image#1").innerHTML = 'Image #1';
    document.getElementById("image#2").innerHTML = '';
    document.getElementById("image#3").innerHTML = '';
    document.getElementById("image#4").innerHTML = '';   

    document.getElementById("img1").src = "optimal_fit.PNG";
    document.getElementById("img1").style.display = "inline";
    document.getElementById("img2").src = "";
    document.getElementById("img3").src = "";
    document.getElementById("img4").src = "";
    document.getElementById("img5").src = "";
}

/*2 -- width tight, height exact -- 1) tight fit   2) overtighten screw */
function msg2(inside_outside){
//  getMessages();
    document.getElementById("results1").style.visibility= "visible";
    document.getElementById("results2").style.visibility= "visible";
    document.getElementById("results3").style.visibility= "visible";
    document.getElementById("results4").style.visibility= "visible";
    document.getElementById("results5").style.visibility= "visible";
    document.getElementById("results6").style.visibility= "visible";
    document.getElementById("results7").style.visibility= "visible";
    document.getElementById("results8").style.visibility= "visible";
    document.getElementById("results9").style.visibility= "visible";
    document.getElementById("box1").style.visibility= "visible";
    document.getElementById("box2").style.visibility= "visible";

    document.getElementById("box1").style.backgroundColor = "rgb(255, 247, 136)";
    document.getElementById("box2").style.backgroundColor = "rgb(255, 247, 136)";

  //  document.getElementById("num").innerHTML = '2';
  document.getElementById("instock/custom").innerHTML = 'Good News!!<br/> Your measurement is either a Standard or In-Stock Custom Item.';
  document.getElementById("msg1").innerHTML = '<strong><u>Tight Fit</u></strong> <br/>Please note that your ' + inside_outside + ' width measurement is close to the maximum skylight clearance making for a tight fit. (See image #1) Since the skylight overhangs the outside curb/flashing (like a shoebox lid), we recommend that you verify: <br/>•  That the curb is in square <br/>•  That the flashed outside curb dimension will allow the appropriate room so the skylight can fit over the flashed curb to ensure a proper seal of the inner skylight gasket to the top of the curb <br/><br/>Tip: When mounting the included screws, please ensure you do not overtighten and bend the aluminum overhang inwards. (See image #2)';
  document.getElementById("msg2").innerHTML = '';

 /* document.getElementById("instock/custom").innerHTML = instock_custom[1];
    document.getElementById("msg1").innerHTML = msg_1[1];
    document.getElementById("msg2").innerHTML = msg_2[1];*/
  

    document.getElementById("image#1").innerHTML = 'Image #1';
    document.getElementById("image#2").innerHTML = 'Image #2';
    document.getElementById("image#3").innerHTML = '';
    document.getElementById("image#4").innerHTML = '';

    document.getElementById("img5").src = "";
    document.getElementById("img4").src = "";
    document.getElementById("img1").src = "tight_fit.PNG";
    document.getElementById("img1").style.display = "inline";
    document.getElementById("img3").src = "";
    document.getElementById("img2").src = "overtighten_screw.PNG";
    document.getElementById("img2").style.display = "inline";
}

/*3 -- width exact, height tight -- 1) tight fit  2) overtighten screw */
function msg3(inside_outside){
 // getMessages();
    document.getElementById("results1").style.visibility= "visible";
    document.getElementById("results2").style.visibility= "visible";
    document.getElementById("results3").style.visibility= "visible";
    document.getElementById("results4").style.visibility= "visible";
    document.getElementById("results5").style.visibility= "visible";
    document.getElementById("results6").style.visibility= "visible";
    document.getElementById("results7").style.visibility= "visible";
    document.getElementById("results8").style.visibility= "visible";
    document.getElementById("results9").style.visibility= "visible";
    document.getElementById("box1").style.visibility= "visible";
    document.getElementById("box2").style.visibility= "visible";

    document.getElementById("box1").style.backgroundColor = "rgb(255, 247, 136)";
    document.getElementById("box2").style.backgroundColor = "rgb(255, 247, 136)";

  //  document.getElementById("num").innerHTML = '3';
  document.getElementById("instock/custom").innerHTML = 'Good News!!<br/> Your measurement is either a Standard or In-Stock Custom Item.';
  document.getElementById("msg1").innerHTML = '<strong><u>Tight Fit</u></strong> <br/>Please note that your ' + inside_outside + ' height measurement is close to the maximum skylight clearance making for a tight fit. (See image #1) Since the skylight overhangs the outside curb/flashing (like a shoebox lid), we recommend that you verify: <br/>•  That the curb is in square <br/>•  That the flashed outside curb dimension will allow the appropriate room so the skylight can fit over the flashed curb to ensure a proper seal of the inner skylight gasket to the top of the curb <br/><br/>Tip: When mounting the included screws, please ensure you do not overtighten and bend the aluminum overhang inwards. (See image #2)';
  document.getElementById("msg2").innerHTML = '';  

    document.getElementById("image#1").innerHTML = 'Image #1';
    document.getElementById("image#2").innerHTML = 'Image #2';
    document.getElementById("image#3").innerHTML = '';
    document.getElementById("image#4").innerHTML = '';

    document.getElementById("img5").src = "";
    document.getElementById("img4").src = "";
    document.getElementById("img1").src = "tight_fit.PNG";
    document.getElementById("img1").style.display = "inline";
    document.getElementById("img3").src = "";
    document.getElementById("img2").src = "overtighten_screw.PNG";
    document.getElementById("img2").style.display = "inline";
}

/*4 -- width tight, height tight -- 1) tight fit  2) overtighten screw*/
function msg4(inside_outside){
 // getMessages();
    document.getElementById("results1").style.visibility= "visible";
    document.getElementById("results2").style.visibility= "visible";
    document.getElementById("results3").style.visibility= "visible";
    document.getElementById("results4").style.visibility= "visible";
    document.getElementById("results5").style.visibility= "visible";
    document.getElementById("results6").style.visibility= "visible";
    document.getElementById("results7").style.visibility= "visible";
    document.getElementById("results8").style.visibility= "visible";
    document.getElementById("results9").style.visibility= "visible";
    document.getElementById("box1").style.visibility= "visible";
    document.getElementById("box2").style.visibility= "visible";

    document.getElementById("box1").style.backgroundColor = "rgb(255, 247, 136)";
    document.getElementById("box2").style.backgroundColor = "rgb(255, 247, 136)";

  //  document.getElementById("num").innerHTML = '4';
  document.getElementById("instock/custom").innerHTML = 'Good News!!<br/> Your measurement is either a Standard or In-Stock Custom Item.';
  document.getElementById("msg1").innerHTML = '<strong><u>Tight Fit</u></strong> <br/>Please note that your ' + inside_outside + ' width and/or height measurements are close to the maximum skylight clearance making for a tight fit. (See image #1) Since the skylight overhangs the outside curb/flashing (like a shoebox lid), we recommend that you verify: <br/>•  That the curb is in square <br>•  That the flashed outside curb dimension will allow the appropriate room so the skylight can fit over the flashed curb to ensure a proper seal of the inner skylight gasket to the top of the curb <br/><br/>Tip: When mounting the included screws, please ensure you do not overtighten and bend the aluminum overhang inwards. (See image #2)';
  document.getElementById("msg2").innerHTML = '';

    document.getElementById("image#1").innerHTML = 'Image #1';
    document.getElementById("image#2").innerHTML = 'Image #2';
    document.getElementById("image#3").innerHTML = '';
    document.getElementById("image#4").innerHTML = '';

    document.getElementById("img5").src = "";
    document.getElementById("img4").src = "";
    document.getElementById("img1").src = "tight_fit.PNG";
    document.getElementById("img1").style.display = "inline";
    document.getElementById("img3").src = "";
    document.getElementById("img2").src = "overtighten_screw.PNG";
    document.getElementById("img2").style.display = "inline";
}

/*5 -- width loose, height exact -- 1) loose fit  2)  loose fit w/ backer rod  3) overtighten screw*/
function msg5(inside_outside){ //FIXED
 // getMessages();
    document.getElementById("results1").style.visibility= "visible";
    document.getElementById("results2").style.visibility= "visible";
    document.getElementById("results3").style.visibility= "visible";
    document.getElementById("results4").style.visibility= "visible";
    document.getElementById("results5").style.visibility= "visible";
    document.getElementById("results6").style.visibility= "visible";
    document.getElementById("results7").style.visibility= "visible";
    document.getElementById("results8").style.visibility= "visible";
    document.getElementById("results9").style.visibility= "visible";
    document.getElementById("box1").style.visibility= "visible";
    document.getElementById("box2").style.visibility= "visible";

    document.getElementById("box1").style.backgroundColor = "rgb(255, 247, 136)";
    document.getElementById("box2").style.backgroundColor = "rgb(255, 247, 136)";

  //  document.getElementById("num").innerHTML = '5';
  document.getElementById("instock/custom").innerHTML = 'Good News!!<br/> Your measurement is either a Standard or In-Stock Custom Item.';
  document.getElementById("msg1").innerHTML = '<strong><u>Loose Fit</u></strong> <br/>Please note that your ' + inside_outside + ' width measurement is close to the minimum curb dimension, leaving a gap between the skylight overhang and the flashing. (See image #1) To ensure you have a proper seal, we recommend: <br/>•  Properly centering the skylight side-to-side and top-to-bottom on the curb to maximize coverage of the inner gasket on top of the curb <br/>•  Where there is a large gap (>1/2”), using a foam backer rod tucked between the flashing and skylight overhang. (See image#2)<br/><br/>Tip: When mounting the included screws, please ensure you do not overtighten and bend the aluminum overhang inwards. (See image #3)';
  document.getElementById("msg2").innerHTML = '';

    document.getElementById("image#1").innerHTML = 'Image #1';
    document.getElementById("image#2").innerHTML = 'Image #2';
    document.getElementById("image#3").innerHTML = 'Image #3';
    document.getElementById("image#4").innerHTML = '';

    document.getElementById("img5").src = "";
    document.getElementById("img1").src = "loose_fit.PNG";
    document.getElementById("img1").style.display = "inline";
    document.getElementById("img4").src = "";
    document.getElementById("img2").src = "loose_fit_w_backer_rod.PNG";
    document.getElementById("img2").style.display = "inline";
    document.getElementById("img3").src = "overtighten_screw.PNG";
    document.getElementById("img3").style.display = "inline";
}

/*6 -- width exact, height loose -- 1) loose fit  2)  loose fit w/ backer rod  3) overtighten screw*/
function msg6(inside_outside){
 // getMessages();
    document.getElementById("results1").style.visibility= "visible";
    document.getElementById("results2").style.visibility= "visible";
    document.getElementById("results3").style.visibility= "visible";
    document.getElementById("results4").style.visibility= "visible";
    document.getElementById("results5").style.visibility= "visible";
    document.getElementById("results6").style.visibility= "visible";
    document.getElementById("results7").style.visibility= "visible";
    document.getElementById("results8").style.visibility= "visible";
    document.getElementById("results9").style.visibility= "visible";
    document.getElementById("box1").style.visibility= "visible";
    document.getElementById("box2").style.visibility= "visible";

    document.getElementById("box1").style.backgroundColor = "rgb(255, 247, 136)";
    document.getElementById("box2").style.backgroundColor = "rgb(255, 247, 136)";

  //  document.getElementById("num").innerHTML = '6';
  document.getElementById("instock/custom").innerHTML = 'Good News!!<br/> Your measurement is either a Standard or In-Stock Custom Item.';
  document.getElementById("msg1").innerHTML = '<strong><u>Loose Fit</u></strong> <br/>Please note that your ' + inside_outside + ' height measurement is close to the minimum curb dimension, leaving a gap between the skylight overhang and the flashing. (See image #1) To ensure you have a proper seal, we recommend: <br/>•  Properly centering the skylight side-to-side and top-to-bottom on the curb to maximize coverage of the inner gasket on top of the curb <br/>•  Where there is a large gap (>1/2”), using a foam backer rod tucked between the flashing and skylight overhang. (See image#2) <br/><br/>Tip: When mounting the included screws, please ensure you do not overtighten and bend the aluminum overhang inwards. (See image #3)';
  document.getElementById("msg2").innerHTML = '';

    document.getElementById("image#1").innerHTML = 'Image #1';
    document.getElementById("image#2").innerHTML = 'Image #2';
    document.getElementById("image#3").innerHTML = 'Image #3';
    document.getElementById("image#4").innerHTML = '';

    document.getElementById("img5").src = "";
    document.getElementById("img1").src = "loose_fit.PNG";
    document.getElementById("img1").style.display = "inline";
    document.getElementById("img4").src = "";
    document.getElementById("img2").src = "loose_fit_w_backer_rod.PNG";
    document.getElementById("img2").style.display = "inline";
    document.getElementById("img3").src = "overtighten_screw.PNG";
    document.getElementById("img3").style.display = "inline";
}

/*7 -- width loose, height loose -- 1) loose fit  2)  loose fit w/ backer rod  3) overtighten screw*/
function msg7(inside_outside){
 // getMessages();
    document.getElementById("results1").style.visibility= "visible";
    document.getElementById("results2").style.visibility= "visible";
    document.getElementById("results3").style.visibility= "visible";
    document.getElementById("results4").style.visibility= "visible";
    document.getElementById("results5").style.visibility= "visible";
    document.getElementById("results6").style.visibility= "visible";
    document.getElementById("results7").style.visibility= "visible";
    document.getElementById("results8").style.visibility= "visible";
    document.getElementById("results9").style.visibility= "visible";
    document.getElementById("box1").style.visibility= "visible";
    document.getElementById("box2").style.visibility= "visible";

    document.getElementById("box1").style.backgroundColor = "rgb(255, 247, 136)";
    document.getElementById("box2").style.backgroundColor = "rgb(255, 247, 136)";

   // document.getElementById("num").innerHTML = '7';
   document.getElementById("instock/custom").innerHTML = 'Good News!!<br/> Your measurement is either a Standard or In-Stock Custom Item.';
   document.getElementById("msg1").innerHTML = '<strong><u>Loose Fit</u></strong> <br/>Please note that your ' + inside_outside + ' width and/or height measurements are close to the minimum curb dimension, leaving a gap between the skylight overhang and the flashing. (See image #1) To ensure you have a proper seal, we recommend: <br/>•  Properly centering the skylight side-to-side and top-to-bottom on the curb to maximize coverage of the inner gasket on top of the curb <br>•  Where there is a large gap (>1/2”), using a foam backer rod tucked between the flashing and skylight overhang. (See image#2) <br/><br/>Tip: When mounting the included screws, please ensure you do not overtighten and bend the aluminum overhang inwards. (See image #3)';
   document.getElementById("msg2").innerHTML = '';

    document.getElementById("image#1").innerHTML = 'Image #1';
    document.getElementById("image#2").innerHTML = 'Image #2';
    document.getElementById("image#3").innerHTML = 'Image #3';
    document.getElementById("image#4").innerHTML = '';

    document.getElementById("img5").src = "";
    document.getElementById("img1").src = "loose_fit.PNG";
    document.getElementById("img1").style.display = "inline";
    document.getElementById("img4").src = "";
    document.getElementById("img2").src = "loose_fit_w_backer_rod.PNG";
    document.getElementById("img2").style.display = "inline";
    document.getElementById("img3").src = "overtighten_screw.PNG";
    document.getElementById("img3").style.display = "inline";
}

/*8 -- width tight, height loose -- 1) tight fit  2) overtighten screw  3) loose fit  4) loose fit w/ backer rod*/
function msg8(inside_outside){
///  getMessages();
    document.getElementById("results1").style.visibility= "visible";
    document.getElementById("results2").style.visibility= "visible";
    document.getElementById("results3").style.visibility= "visible";
    document.getElementById("results4").style.visibility= "visible";
    document.getElementById("results5").style.visibility= "visible";
    document.getElementById("results6").style.visibility= "visible";
    document.getElementById("results7").style.visibility= "visible";
    document.getElementById("results8").style.visibility= "visible";
    document.getElementById("results9").style.visibility= "visible";
    document.getElementById("box1").style.visibility= "visible";
    document.getElementById("box2").style.visibility= "visible";

    document.getElementById("box1").style.backgroundColor = "rgb(255, 247, 136)";
    document.getElementById("box2").style.backgroundColor = "rgb(255, 247, 136)";

  //  document.getElementById("num").innerHTML = '8';
  document.getElementById("instock/custom").innerHTML = 'Good News!!<br/> Your measurement is either a Standard or In-Stock Custom Item.';
  document.getElementById("msg1").innerHTML = '<strong><u>Tight Fit</u></strong> <br/>Please note that your ' + inside_outside + ' width measurement is close to the maximum skylight clearance making for a tight fit. (See image #1) Since the skylight overhangs the outside curb/flashing (like a shoebox lid), we recommend that you verify <br/>•  That the curb is in square <br/>•  That the flashed outside curb dimension will allow the appropriate room so the skylight can fit over the flashed curb to ensure a proper seal of the inner skylight gasket to the top of the curb <br/><br/>Tip: When mounting the included screws, please ensure you do not overtighten and bend the aluminum overhang inwards. (See image #2)';
  document.getElementById("msg2").innerHTML = '<strong><u>Loose Fit</u></strong> <br/>Please note that your ' + inside_outside + ' height measurement is close to the minimum curb dimension, leaving a gap between the skylight overhang and the flashing. (See image #3) To ensure you have a proper seal, we recommend: <br/>•  Properly centering the skylight side-to-side and top-to-bottom on the curb to maximize coverage of the inner gasket on top of the curb <br/>•  Where there is a large gap (>1/2”), using a foam backer rod tucked between the flashing and skylight overhang. (See image#4) <br/><br/>Tip: When mounting the included screws, please ensure you do not overtighten and bend the aluminum overhang inwards. (See image #2)';

    document.getElementById("image#1").innerHTML = 'Image #1';
    document.getElementById("image#2").innerHTML = 'Image #2';
    document.getElementById("image#3").innerHTML = 'Image #3';
    document.getElementById("image#4").innerHTML = 'Image #4';

    document.getElementById("img5").src = "";
    document.getElementById("img3").src = "loose_fit.PNG";
    document.getElementById("img3").style.display = "inline";
    document.getElementById("img1").src = "tight_fit.PNG";
    document.getElementById("img1").style.display = "inline";
    document.getElementById("img4").src = "loose_fit_w_backer_rod.PNG";
    document.getElementById("img4").style.display = "inline";
    document.getElementById("img2").src = "overtighten_screw.PNG";
    document.getElementById("img2").style.display = "inline";
}

/*9 -- width loose, height tight -- 1) tight fit  2) overtighten screw  3) loose fit  4) loose fit w/ backer rod*/
function msg9(inside_outside){
 // getMessages();
    document.getElementById("results1").style.visibility= "visible";
    document.getElementById("results2").style.visibility= "visible";
    document.getElementById("results3").style.visibility= "visible";
    document.getElementById("results4").style.visibility= "visible";
    document.getElementById("results5").style.visibility= "visible";
    document.getElementById("results6").style.visibility= "visible";
    document.getElementById("results7").style.visibility= "visible";
    document.getElementById("results8").style.visibility= "visible";
    document.getElementById("results9").style.visibility= "visible";

    document.getElementById("box1").style.visibility= "visible";
    document.getElementById("box2").style.visibility= "visible";

    document.getElementById("box1").style.backgroundColor = "rgb(255, 247, 136)";
    document.getElementById("box2").style.backgroundColor = "rgb(255, 247, 136)";

   // document.getElementById("num").innerHTML = '9';
   document.getElementById("instock/custom").innerHTML = 'Good News!!<br/> Your measurement is either a Standard or In-Stock Custom Item.';
   document.getElementById("msg1").innerHTML = '<strong><u>Loose Fit</u></strong> <br/>Please note that your ' + inside_outside + ' height measurement is close to the minimum curb dimension, leaving a gap between the skylight overhang and the flashing. (See image #1) To ensure you have a proper seal, we recommend: <br/>•  Properly centering the skylight side-to-side and top-to-bottom on the curb to maximize coverage of the inner gasket on top of the curb <br/>•  Where there is a large gap (>1/2”), using a foam backer rod tucked between the flashing and skylight overhang. (See image#2) <br/><br/>Tip: When mounting the included screws, please ensure you do not overtighten and bend the aluminum overhang inwards. (See image #3)';
   document.getElementById("msg2").innerHTML = '<strong><u>Tight Fit</u></strong> <br/>Please note that your ' + inside_outside + ' width measurement is close to the maximum skylight clearance making for a tight fit. (See image #4) Since the skylight overhangs the outside curb/flashing (like a shoebox lid), we recommend that you verify <br/>•  That the curb is in square <br/>•  That the flashed outside curb dimension will allow the appropriate room so the skylight can fit over the flashed curb to ensure a proper seal of the inner skylight gasket to the top of the curb <br/><br/>Tip: When mounting the included screws, please ensure you do not overtighten and bend the aluminum overhang inwards. (See image #3)';
   
  /* document.getElementById("instock/custom").innerHTML = instock_custom[8];
    document.getElementById("msg1").innerHTML = msg_1[8];
    document.getElementById("msg2").innerHTML = msg_2[8];*/

    document.getElementById("image#1").innerHTML = 'Image #1';
    document.getElementById("image#2").innerHTML = 'Image #2';
    document.getElementById("image#3").innerHTML = 'Image #3';
    document.getElementById("image#4").innerHTML = 'Image #4';

    document.getElementById("img5").src = "";
    document.getElementById("img1").src = "loose_fit.PNG";
    document.getElementById("img1").style.display = "inline";
    document.getElementById("img3").src = "tight_fit.PNG";
    document.getElementById("img3").style.display = "inline";
    document.getElementById("img2").src = "loose_fit_w_backer_rod.PNG";
    document.getElementById("img2").style.display = "inline";
    document.getElementById("img4").src = "overtighten_screw.PNG";
    document.getElementById("img4").style.display = "inline";
}

/*if neither, skylight does not exist, order custom*/
function msg10(inside_outside){
  getMessages();
    document.getElementById("results1").style.visibility= "hidden";
    document.getElementById("results2").style.visibility= "hidden";
    document.getElementById("results3").style.visibility= "hidden";
    document.getElementById("results4").style.visibility= "hidden";
    document.getElementById("results5").style.visibility= "hidden";
    document.getElementById("results6").style.visibility= "hidden";
    document.getElementById("results7").style.visibility= "hidden";
    document.getElementById("results8").style.visibility= "hidden";
    document.getElementById("results9").style.visibility= "hidden";
    document.getElementById("box1").style.visibility= "visible";
    document.getElementById("box2").style.visibility= "visible";

   /* document.getElementById("results").style.display= "block";
    document.getElementById("results1").style.display= "block";
    document.getElementById("results2").style.display= "block";
    document.getElementById("results3").style.display= "block";
    document.getElementById("results4").style.display= "block";
    document.getElementById("results5").style.display= "block";
    document.getElementById("results6").style.display= "block";
    document.getElementById("results7").style.display= "block";
    document.getElementById("results8").style.display= "block";
    document.getElementById("results9").style.display= "block";*/

    document.getElementById("box1").style.backgroundColor = "rgb(249, 141, 141)";
    document.getElementById("box2").style.backgroundColor = "rgb(249, 141, 141)";

   // document.getElementById("num").innerHTML = '10';
   document.getElementById("instock/custom").innerHTML = 'Unfortunately, your item is not a stock item.';
   document.getElementById("msg1").innerHTML =
   `<p>A custom order is required. <a href="prices.html" target="_blank"><strong><u>Click here for more FCM pricing options</u></strong></a> OR Call 1-800-888-3589 for pricing</p>`;
   document.getElementById("msg2").innerHTML = '';
   
    document.getElementById("image#1").innerHTML = '';
    document.getElementById("image#2").innerHTML = '';
    document.getElementById("image#3").innerHTML = '';
    document.getElementById("image#4").innerHTML = '';

    document.getElementById("img1").src = "";
    document.getElementById("img2").src = "";
    document.getElementById("img3").src = "";
    document.getElementById("img4").src = "";
    document.getElementById("img5").src = "";
}
